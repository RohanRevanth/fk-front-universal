import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders }  from '@angular/core';
import { HomeComponent } from './pages/home';
import { ProfileComponent } from "./pages/profile/profile.component";
import { IndustryComponent } from './pages/industry/industry.component';
import { ProductPageComponent } from "./pages/product-page/product-page.component";
import { FashionComponent } from "./pages/fashion/fashion.component";
import { FeedComponent } from "./pages/feed/feed.component";
import { ContentPageComponent } from "./pages/content/content.component";
import { DummyComponent } from "./pages/dummy/dummy.component";
import { AuctionPageComponent } from './pages/auction/auction-page.component';
import { UserProfileComponent } from './pages/userprofile/userprofile.component';

import { SeeAllComponent } from "./pages/see-all/see-all.component";
import { CheckoutComponent } from "./pages/checkout/checkout.component";
import { BrandStoreComponent } from "./pages/brandstore/brandstore.component";
import { EditProfileComponent } from "./pages/edit-profile/edit-profile.component";
import { StaticPageComponent } from './pages/static-page/static-page.component';



export const ROUTES: Routes = [
  { path: '',      component: HomeComponent },
  { path: 'home',  component: HomeComponent },
  { path: 'staticpage', component: StaticPageComponent },
  { path: 'industry/tollywood', component: IndustryComponent },
  { path: 'industry/bollywood', component: IndustryComponent },
  { path: 'industry/kollywood', component: IndustryComponent },
  { path: 'industry/mollywood', component: IndustryComponent },
  { path: 'industry/sandalwood', component: IndustryComponent },
  { path: 'movie/:slug', component: ProfileComponent },
  { path: 'celeb/:slug', component: ProfileComponent },
  { path: 'designer/:slug', component: ProfileComponent },

  
  { path: 'celeb-store', component: FashionComponent},
  { path: 'celeb-store/:slug', component: ProfileComponent},
  { path: 'celebstore/:slug/:slug', component: SeeAllComponent},
  { path: 'celebstore/:slug', component: SeeAllComponent},

  { path: 'movie-store', component: FashionComponent},
  { path: 'movie-store/:slug', component: ProfileComponent},
  { path: 'moviestore/:slug/:slug', component: SeeAllComponent},
  { path: 'moviestore/:slug', component: SeeAllComponent},
  
 
  { path: 'product/:slug', component: ProductPageComponent },
  { path: 'product/:slug/:slug', component: ProductPageComponent },
  { path: 'looks/:slug', component: ProductPageComponent },
  { path: 'menfashion/:slug/:slug/:slug', component: SeeAllComponent},
  { path: 'menfashion/:slug/:slug', component: SeeAllComponent},
  { path: 'menfashion/:slug', component: SeeAllComponent},
  { path: 'menfashion', component: FashionComponent},
  
  { path: 'womenfashion/:slug/:slug', component: SeeAllComponent},
  { path: 'womenfashion/:slug/:slug/:slug', component: SeeAllComponent},
  { path: 'womenfashion/:slug', component: SeeAllComponent},
  { path: 'womenfashion', component: FashionComponent},
  
  { path: 'designer-store', component: FashionComponent},
  { path: 'designer-store/:slug', component: FashionComponent},
  { path: 'designerstore/:slug/:slug', component: SeeAllComponent},
  { path: 'designerstore/:slug', component: SeeAllComponent},

  { path: 'feed/:slug', component: FeedComponent},
  { path: 'feed/:slug/:slug', component: FeedComponent},
  { path: 'feed', component: FeedComponent},
  { path: 'boutique', component: FeedComponent},
  { path: 'bookmark', component: FeedComponent},
  { path: 'like', component: FeedComponent},
  { path: 'orders', component: FeedComponent},
  { path: 'trending', component: FeedComponent},

  { path: 'share-your-style', component: FeedComponent},
  // { path: 'content/gallery', component: ContentPageComponent},
  // { path: 'content/dialouge', component: ContentPageComponent},
  // { path: 'content/nowshowing', component: ContentPageComponent},
  // { path: 'content/up-comming', component: ContentPageComponent},

  { 
    path: 'content/:slug', 
    component: ContentPageComponent,
    data: {
      meta: {
        title: 'Flikster',
        description: 'Flikster - Movies & Fashion',
        // 'og:image': 'https://bucket-for-image-test.s3.amazonaws.com/flikstre-logo2.png'
      }
    }
  },
  { path: 'dummy', component: DummyComponent},
  { path: 'auctions', component: AuctionPageComponent},
  { path: 'auctions/:slug', component: AuctionPageComponent},
  { path: 'user', component:UserProfileComponent},
  { path: 'editprofile', component:EditProfileComponent},
  { path: 'see-all', component:SeeAllComponent},
  { path: 'see-all/:slug/:slug', component:SeeAllComponent},
  { path: 'checkout/:slug', component:CheckoutComponent},
  { path: 'auction/checkout/:slug', component:CheckoutComponent},
 { path: 'brand-store/:slug', component:BrandStoreComponent},

  { path: 'privacy', component:StaticPageComponent},
  { path: 'terms', component:StaticPageComponent},
  { path: 'about', component:StaticPageComponent},
  { path: 'careers', component:StaticPageComponent},
  { path: 'refer-a-friend', component:StaticPageComponent},
  { path: 'comingsoon', component:StaticPageComponent},
  { path: 'rewards', component:StaticPageComponent},
  { path: 'return-policy', component:StaticPageComponent},
  { path: 'faq', component:StaticPageComponent},
  { path: 'help', component:StaticPageComponent},
  { path: 'blog', component:StaticPageComponent},
  { path: 'business', component:StaticPageComponent},
  { path: 'contact', component:StaticPageComponent},
  { path: 'auctionFaqs', component:StaticPageComponent},
  

];

export const appRoutingProviders: any[] = [

];
export const routing: ModuleWithProviders = RouterModule.forRoot(ROUTES);
