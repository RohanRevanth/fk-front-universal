import {
    Component,
    OnInit, Input
  } from '@angular/core';
  
  // import { AppState } from '../../app.service';
  import { ActivatedRoute, Router } from "@angular/router";
  
  @Component({
    
    selector: 'card-dialogue', 
    providers: [ ],
    styleUrls: [ './card-dialogue.component.css' ],
    templateUrl: './card-dialogue.component.html'
  })
  export class CardDialogueComponent implements OnInit {
    page: string;
    shareProfilePic: any;
    shareSlug: any;
    time: string;
    ShowTime: number;
    milisecondsDiff: number;
    miliseconds: number;
    endDate: any;
    startDate: any;
    createdAt: any;
    public localState = { value: '' };
    
    @Input() miniCard : boolean
    @Input() content : any;
    public isMiniCard : boolean = false;
    public contentData : any;
    public assnData : any;
    public cardId : any;
    public audioUrl : any;
    public cardSlug :any;
    public isArray : boolean = false;
    public today: number = Date.now();

    constructor(
      // public appState: AppState,
      public router: Router,
    ) {}
  
    public ngOnInit() {
      var urlBreakup = this.router.url.split('/');
      this.page = urlBreakup[1];
      this.cardSlug=urlBreakup[2];
      console.log('hello `card-dialogue` component');
       this.contentData = this.content;
       this.cardId = this.contentData.id;
      this.shareSlug=this.contentData.slug;
      this.shareProfilePic = this.contentData.profilePic;
       this.createdAt=this.content.createdAt;
      if(typeof this.contentData.media.audio == 'string') {
        this.audioUrl = this.contentData.media.audio;
      } else {
        this.isArray = true;
         this.audioUrl = this.contentData.media.audio[0];
      }
       if(this.contentData.celeb)
        this.assnData=this.contentData.celeb[0];
        if(this.contentData.movie)
          this.assnData=this.contentData.movie[0];
      // this.assnData = this.content.assnData;
      if(this.miniCard) {
        this.isMiniCard = this.miniCard;
      }
      this.startDate = new Date( this.createdAt);
      this.endDate = new Date(this.today);
      this.miliseconds = this.endDate - this.startDate;
      this.milisecondsDiff=this.miliseconds / 1000 /60 /60 /24/7;  
      if(this.milisecondsDiff >=1){
        this.ShowTime= Math.floor(this.milisecondsDiff); 
        this.time='w';
      }
      else {
        this.milisecondsDiff=this.miliseconds / 1000 / 60 /60/24; 
        if(this.milisecondsDiff >=1){
        this.ShowTime= Math.floor(this.milisecondsDiff); 
        this.time='d';
        }
        else{
          this.milisecondsDiff=this.miliseconds/ 1000 / 60 /60 ;
          if(this.milisecondsDiff >=1){
            this.ShowTime= Math.floor(this.milisecondsDiff);
            this.time='h';
          }
          else{
            this.milisecondsDiff=this.miliseconds /1000/60;
            if(this.milisecondsDiff >=1){
            this.ShowTime= Math.floor(this.milisecondsDiff);
            this.time='m';
            }
            else{
              this.milisecondsDiff=this.miliseconds /1000;
              this.ShowTime= Math.floor(this.milisecondsDiff);
              this.time='sec';
            }
          } 

        }


      }   
      console.log(this.ShowTime,this.time);
    }
  
    public submitState(value: string) {
      console.log('submitState', value);
      // this.appState.set('value', value);
      this.localState.value = '';
    }
    gotocontent(){
      if(this.cardSlug != this.contentData.slug){
      this.router.navigateByUrl('/dummy', {skipLocationChange: true}).then(()=>
      this.router.navigate(['content/' + this.contentData.slug]));
    }
    }
  }
  