import { Component, OnInit, Input} from '@angular/core';

//import { AppState } from '../app.service';
// import { AppState } from '../../app.service';

@Component({
  
  selector: 'card-related-flicks', 
  providers: [ ],
  styleUrls: [ './card-related-flicks.component.css' ],
  templateUrl: './card-related-flicks.component.html'
})
export class RelatedFlicksComponent implements OnInit {
  public localState = { value: '' };
  
 // @Input() content : any;
  @Input() pageType5: any;
  
  private id: string = '-twi5MBq1TQ';
  private height: number = 460;
  private width: any = '100%';

  public contentData : any;

  
 
    
  constructor(
    // public appState: AppState,
  ) {}

  public ngOnInit() {
    console.log('hello `related-flicks` component');
    console.log(this.pageType5);
    this.contentData = this.pageType5.content;
    
  }

  public submitState(value: string) {
    console.log('submitState', value);
    // this.appState.set('value', value);
    this.localState.value = '';
  }
}
