import {
    Component,
    OnInit, Input
  } from '@angular/core';
  
  // import { AppState } from '../../app.service';
  
  @Component({
    
    selector: 'card-style',   
    providers: [ ],
    styleUrls: [ './card-style.component.css' ],
    templateUrl: './card-style.component.html'
  })
  export class StyleCardComponent implements OnInit {
    shareProfilePic: any;
    shareSlug: any;
    // isMiniCard: boolean;
    @Input() miniCard:any;
    @Input() content: any;
    @Input() cardTypeData : any;
    public cardtype;
    public localState = { value: '' };
    public assnData;
    public isUser;
    public timeDiff;
    public cardId;
    constructor(
      // public appState: AppState,
    ) {}
  
    public ngOnInit() {
      console.log(this.content);
      this.isUser = true;
      this.cardtype = this.content.styleTemplate;
      this.assnData = this.content.userObject;
      this.cardId = this.content.id;
      this.shareSlug=this.content.slug;
      this.shareProfilePic = this.content.profilePic;
      
      let timeNow = new Date();
      let createdDate = new Date(this.content.createdAt.toString());
      
      // if(!this.content.userObject) {
      //   this.assnData = {
      //     username : "Gal Gadot",
      //     profilePic : "https://upload.wikimedia.org/wikipedia/commons/thumb/5/5b/Gal_Gadot_cropped_lighting_corrected_2b.jpg/220px-Gal_Gadot_cropped_lighting_corrected_2b.jpg"
      //   }
      // }


       
    }
  
    public submitState(value: string) {
      console.log('submitState', value);
      // this.appState.set('value', value);
      this.localState.value = '';
    }
  }
  