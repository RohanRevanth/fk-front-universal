import {
    Component,
    OnInit,
    Input
  } from '@angular/core';
  
  // import { AppState } from '../../app.service';
  
  @Component({
    
    selector: 'up-comming', 
    providers: [ ],
    styleUrls: [ './up-comming.component.css' ],
    templateUrl: './up-comming.component.html'
  })
  export class UpCommingComponent implements OnInit {
    isMiniCard: any;
    @Input() content : any;
    @Input() miniCard: any;
    public localState = { value: '' };
    public contentData;
    constructor(
      // public appState: AppState,
    ) {}
  
    public ngOnInit() {
      console.log('hello `up-comming` component');
      this.contentData = this.content;
      if(this.miniCard) {
        this.isMiniCard = this.miniCard;
      }
    }
  
    public submitState(value: string) {
      console.log('submitState', value);
      // this.appState.set('value', value);
      this.localState.value = '';
    }
  }