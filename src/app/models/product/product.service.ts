import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
import { Observable } from 'rxjs/Rx';
import { Product } from './product';
import { environment } from "../globals";

@Injectable()
export class ProductService {
    private environmentUrl :  any = environment;
    

  constructor(
        private http: Http,
        private sanitizer: DomSanitizer
    ) { 

  }
  
  
  getProductBySlug(slug) {  
    let productUrl = this.environmentUrl.elasticUrl+'products/_search?pretty=true&q=status:ACTIVE AND slug:"'+slug+'"';
      return this.http.get(productUrl)
  		.map((res:Response) => res.json())
  		.catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
  getProductsByTag(slug) {  
    let productUrl = this.environmentUrl.elasticUrl+'products/_search?pretty=true&sort=createdAt:desc&size=1000&q=status:ACTIVE AND tags:"'+slug+'"';
      return this.http.get(productUrl)
  		.map((res:Response) => res.json())
  		.catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
  searchProductBySlug(slug) {  
    let productUrl = this.environmentUrl.elasticUrl+'products/_search?pretty=true&q=status:ACTIVE AND slug:*'+slug+'*';
      return this.http.get(productUrl)
  		.map((res:Response) => res.json())
  		.catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
  globalSearchRelProducts(slug) {  
    let productUrl = this.environmentUrl.elasticUrl+'products,celebrity,movies,brands,designer/_search?pretty=true&q=status:ACTIVE AND slug:*'+slug+'* OR tags:*'+slug+'*';
      return this.http.get(productUrl)
  		.map((res:Response) => res.json())
  		.catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
  getAllLooks() {  
    
    let productUrl = this.environmentUrl.baseUrl+"looks-ms/looks";
      return this.http.get(productUrl)
  		.map((res:Response) => res.json())
  		.catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
  getAllProducts(tag) {  
    
    let productUrl = this.environmentUrl.elasticUrl+'products/_search?pretty=true&q=status:ACTIVE AND tags:*'+tag+'*';
      return this.http.get(productUrl)
  		.map((res:Response) => res.json())
  		.catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  getAllUserOrders(userId) {  
    let productUrl = this.environmentUrl.elasticUrl+'orders/_search?size=9999&pretty=true&q=userId:"'+userId+'"';
      return this.http.get(productUrl)
  		.map((res:Response) => res.json())
  		.catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
  gethighestbid(slug) {
    
    let productUrl = this.environmentUrl.baseUrl+"auctions-ms/getHighBidAmount/"+slug;
      return this.http.get(productUrl)
  		.map((res:Response) => res.json())
  		.catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
  getOrderById(slug) {
    //let productUrl = this.baseUrl+'product/'+slug;
    let productUrl = this.environmentUrl.baseUrl+"orders-ms/getOrderById/"+slug;
      return this.http.get(productUrl)
  		.map((res:Response) => res.json())
  		.catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
  
  allAuctions(auctions,docsPerPage){  
    let productUrl = this.environmentUrl.baseUrl+"auctions-ms/auctions";
    return this.http.get(productUrl)
    .map((res:Response) => res.json())
    .catch((error:any) => Observable.throw(error.json().error || 'Server Error'))
}
getCompleted() {  
  let productUrl = this.environmentUrl.elasticUrl+'auctions/_search?pretty=true&sort=createdAt:desc&size=1000&q=status:"completed"';
    return this.http.get(productUrl)
    .map((res:Response) => res.json())
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
}
getRelatedAuctions(){  
  let productUrl = this.environmentUrl.baseUrl+"auctions-ms/auctions";
  return this.http.get(productUrl)
  .map((res:Response) => res.json())
  .catch((error:any) => Observable.throw(error.json().error || 'Server Error'))
}

getAuctionProduct(slug){
  let productUrl = this.environmentUrl.baseUrl+"auctions-ms/getAuctionById/"+slug;
  return this.http.get(productUrl)
  .map((res:Response) => res.json())
  .catch((error:any) => Observable.throw(error.json().error || 'Server Error'))
}
getAuctionProductBySlug(slug){
  let productUrl = this.environmentUrl.elasticUrl+'auctions/_search?size=10000&pretty=true&sort=createdAt:desc&size=1000&q=slug:"'+slug+'"';
  return this.http.get(productUrl)
  .map((res:Response) => res.json())
  .catch((error:any) => Observable.throw(error.json().error || 'Server Error'))
}

getLookById(slug){
  let productUrl = this.environmentUrl.baseUrl+"looks-ms/getLookById/"+slug;
  return this.http.get(productUrl)
  .map((res:Response) => res.json())
  .catch((error:any) => Observable.throw(error.json().error || 'Server Error'))

}
getLookBySlug(slug){
  let productUrl = this.environmentUrl.elasticUrl+'looks/_search?size=10000&pretty=true&sort=createdAt:desc&size=1000&q=status:"ACTIVE" AND slug:"'+slug+'"';
  return this.http.get(productUrl)
  .map((res:Response) => res.json())
  .catch((error:any) => Observable.throw(error.json().error || 'Server Error'))

}

postOrder(body) : Observable<any>{
  let ordersUrl = this.environmentUrl.baseUrl+'orders-ms/createOrder';
    let headers = new Headers({ 'Content-Type': 'application/json' });
  //   headers.append('authorization','Bearer'+ " "+localStorage.getItem("token"));
    let options = new RequestOptions({ headers: headers });
    return this.http.post(ordersUrl, body, options)
    .map((res:Response) => res.json())
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
}

addToCart(body) : Observable<any>{
  let ordersUrl = this.environmentUrl.baseUrl+'cart-ms/createCart';
    let headers = new Headers({ 'Content-Type': 'application/json' });
  //   headers.append('authorization','Bearer'+ " "+localStorage.getItem("token"));
    let options = new RequestOptions({ headers: headers });
    return this.http.post(ordersUrl, body, options)
    .map((res:Response) => res.json())
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
}

getCartByUser(userName){
  let productUrl = this.environmentUrl.baseUrl+"cart-ms/getCartByUser/"+userName;
  return this.http.get(productUrl)
  .map((res:Response) => res.json())
  .catch((error:any) => Observable.throw(error.json().error || 'Server Error'))
}
showbiddingData(slug){
  let productUrl = this.environmentUrl.baseUrl+"auctions-ms/getAuctionById/"+slug;
  return this.http.get(productUrl)
  .map((res:Response) => res.json())
  .catch((error:any) => Observable.throw(error.json().error || 'Server Error'))
}
getProductByCategory(fashion,category,type){
  category=category.split("%20").join("");
  type=type.split("%20").join("");
  let productUrl = this.environmentUrl.elasticUrl+'products/_search?size=1000&pretty=true&sort=createdAt:desc&q=status:ACTIVE%20AND%20category:"'+fashion+'"%20AND%20category:"'+category+'"%20AND%20category:"'+type+'"';
  return this.http.get(productUrl)
  .map((res:Response) => res.json())
  .catch((error:any) => Observable.throw(error.json().error || 'Server Error'))
}
// deleteItem(id){
//   let productUrl = "http://apiv3.flikster.com/v3/cart-ms/removeItemFromCart/"+id;
//   return this.http.put(productUrl)
//   .map((res:Response) => res.json())
//   .catch((error:any) => Observable.throw(error.json().error || 'Server Error'))
// }
deleteItem(id,body) : Observable<any>{
  let ordersUrl = this.environmentUrl.baseUrl+'cart-ms/removeItemFromCart/'+id;
    let headers = new Headers({ 'Content-Type': 'application/json' });
  //   headers.append('authorization','Bearer'+ " "+localStorage.getItem("token"));
    let options = new RequestOptions({ headers: headers });
    return this.http.get(ordersUrl)
    .map((res:Response) => res.json())
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
}


checkoutOrder(body) : Observable<any>{
  let ordersUrl = this.environmentUrl.baseUrl+'checkout-ms/createCheckout';
    let headers = new Headers({ 'Content-Type': 'application/json' });
  //   headers.append('authorization','Bearer'+ " "+localStorage.getItem("token"));
    let options = new RequestOptions({ headers: headers });
    return this.http.post(ordersUrl, body, options)
    .map((res:Response) => res.json())
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
}

auctionbidding(body) : Observable<Product>{
  let ordersUrl = this.environmentUrl.baseUrl+'auctions-ms/postBidByAuction';
    let headers = new Headers({ 'Content-Type': 'application/json' });
  //   headers.append('authorization','Bearer'+ " "+localStorage.getItem("token"));
    let options = new RequestOptions({ headers: headers });
    return this.http.post(ordersUrl, body, options)
    .map((res:Response) => res.json())
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
}
updateOrders(body,id) : Observable<any>{
  let ordersUrl = this.environmentUrl.baseUrl+'orders-ms/updateOrderById/'+id;
    let headers = new Headers({ 'Content-Type': 'application/json' });
  //   headers.append('authorization','Bearer'+ " "+localStorage.getItem("token"));
    let options = new RequestOptions({ headers: headers });
    return this.http.put(ordersUrl, body, options)
    .map((res:Response) => res.json())
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
}






  

}