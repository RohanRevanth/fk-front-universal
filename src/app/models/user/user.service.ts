import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { environment } from "../globals";

@Injectable()
export class UserService {
  private environmentUrl :  any = environment;
    

  constructor(
  	    private http: Http,
    ) { }

  getFollowSuggestions() {
  	let userUrl = 'http://mock.flikster.com:4000/tollywood/follow-suggestions';
      return this.http.get(userUrl)
  		.map((res:Response) => res.json())
  		.catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  checkEmailOrPhone(type,item){
    let productUrl = this.environmentUrl.baseUrl+'user-ms/'+type+'/'+item;
    return this.http.get(productUrl)
    .map((res:Response) => res.json())
    .catch((error:any) => Observable.throw(error.json().error || 'Server Error'))
  }

  registration(body) : Observable<any>{
    let ordersUrl = this.environmentUrl.baseUrl+'user-ms/registration';
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });
      return this.http.post(ordersUrl, body, options)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  forgotPassword(body) : Observable<any>{
    let ordersUrl = this.environmentUrl.baseUrl+'user-ms/forgotpassword';
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });
      return this.http.post(ordersUrl, body, options)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  changePassword(body) : Observable<any>{
    let ordersUrl = this.environmentUrl.baseUrl+'user-ms/changepassword';
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });
      return this.http.post(ordersUrl, body, options)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  submitOtp(body) : Observable<any>{
    let ordersUrl = this.environmentUrl.baseUrl+'user-ms/checkOtp';
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });
      return this.http.post(ordersUrl, body, options)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  resendOtp(body) : Observable<any>{
    let ordersUrl = this.environmentUrl.baseUrl+'user-ms/resendOtp';
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });
      return this.http.post(ordersUrl, body, options)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  login(body) : Observable<any>{
    let ordersUrl = this.environmentUrl.baseUrl+'user-ms/login';
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });
      return this.http.post(ordersUrl, body, options)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  socialLogin(body) : Observable<any>{
    let ordersUrl = this.environmentUrl.baseUrl+'user-ms/socialReg';
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });
      return this.http.post(ordersUrl, body, options)
      .map((res:Response) => res.json())
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
}
