var devEnvironment = {
  baseUrl : "http://apiv3.flikster.com/v3/",
  elasticUrl : "http://apiv3-es.flikster.com/"
}

var prodEnvironment = {
  baseUrl : "http://apiservice.flikster.com/v3/",
  elasticUrl : "http://apiservice-ec.flikster.com/"
}

var oldAppStoreLinks = {
  googlePlay : "https://play.google.com/store/apps/details?id=com.flikster",
  iTunes : "https://itunes.apple.com/us/app/flikster/id1221940431"
}

var newAppStoreLinks = {
  googlePlay : "https://google.com",
  iTunes : "https://apple.com"
}


//export var environment = devEnvironment;

export var environment = prodEnvironment;


export var appStoreLinks = oldAppStoreLinks;
// export var appStoreLinks = newAppStoreLinks;


