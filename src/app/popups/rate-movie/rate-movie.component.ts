import {
  Component,
  OnInit,
  EventEmitter,
  Input,
  Output
} from '@angular/core';

// import { AppState } from '../../app.service';
import { MovieService } from "../../models/movie/movie.service";

@Component({
  
  selector: 'rate-movie', 
  providers: [ ],
  styleUrls: [ './rate-movie.component.css' ],
  templateUrl: './rate-movie.component.html'
})
export class RateMovieComponent implements OnInit {
    public localState = { value: '' };
    
    @Output() submitingRatingPopup : EventEmitter<any> = new EventEmitter();
    @Input() isReview;
    
    @Input() movieObject;
    @Input() cardId;
    public reviewObject;
    public userrating;
    onClick(value:number): void {
      this.rating = value;
      // this.ratingClicked.emit({
          
      //     rating: this.rating
       };
       public range: Array<number> = [1, 2, 3, 4, 5, 6, 7, 8, 9,10,];
    
       public rating: number=0;
       public title;
       public review;
       public nameofmovie;
    constructor(
      // public appState: AppState,
      public movieService: MovieService,
    ) {}

    public ngOnInit() {
      console.log('hello `rate-movie` component');
      
      if(this.movieObject)
      console.log(this.movieObject);
    }

    closePopup() {
      this.submitingRatingPopup.next();
      if(this.movieObject)
      console.log(this.movieObject);
    }

    submitRating() {
      var userId=localStorage.getItem('username');
      let reviewObject = {
        'entityId':String(this.cardId),
        'ratingValue': String(this.rating),
        'source':'WEBSITE',
        'userId': String(userId),
        'ratingText':this.review,
        'updatedBy': this.title,
       
      }
      this.createrating(reviewObject);
      this.reviewObject={
        
        updatedBy :this.title,
        userId: String(userId),
        ratingValue : this.rating,
        ratingText : this.review,
        // profilePic : "http://js.newsx.com/wp-content/uploads/2017/09/arjun-reddy.jpg"

      }
      this.submitingRatingPopup.emit(this.reviewObject);
    }

    userRating(){
      this.userrating={
        nameofmovie :this.movieObject.title,
        rating : this.rating,
      }
      console.log(this.userrating);
      this.submitingRatingPopup.emit(this.userrating);
      var userId=localStorage.getItem('username');
      let review={
         'entityId':String(this.movieObject.id),
        'ratingValue': String(this.rating),
        'ratingText':'hello',
        'source':'WEBSITE',
        'userId': String(userId),
        
      }
      this.createrating(review);
    }
    createrating(review){
      this.movieService.postReview(review).subscribe(
        res => {
            console.log(res);
            // this.orderid =res.id;
            // this.router.navigate(['checkout/'+this.orderid]);
          },
          error => {
            console.log("Error in posting orders");
          }
    );
  }
    
    public submitState(value: string) {
      console.log('submitState', value);
      // this.appState.set('value', value);
      this.localState.value = '';
    }
}
