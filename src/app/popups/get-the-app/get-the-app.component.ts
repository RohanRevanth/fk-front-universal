import {
  Component,
  OnInit
} from '@angular/core';

// import { AppState } from '../../app.service';

@Component({
  
  selector: 'get-the-app', 
  providers: [ ],
  styleUrls: [ './get-the-app.component.css' ],
  templateUrl: './get-the-app.component.html'
})
export class GetTheAppComponent implements OnInit {
  public localState = { value: '' };

  constructor(
    // public appState: AppState,
  ) {}

  public ngOnInit() {
    console.log('hello `get-the-app` component');
  }

  public submitState(value: string) {
    console.log('submitState', value);
    // this.appState.set('value', value);
    this.localState.value = '';
  }
}
