import {
    Component,
    OnInit, Output, EventEmitter
  } from '@angular/core';
  import { ActivatedRoute, Router } from "@angular/router";
  
  // import { AppState } from '../../app.service';
  import { ContentService } from "../../models/content/content.service";

  @Component({
    
    selector: 'feedbar', 
    providers: [ ],
    styleUrls: [ './feed-bar.component.css' ],
    templateUrl: './feed-bar.component.html'
  })
  export class FeedBarComponent implements OnInit {
    index: string;
    contentType: string;
    industry: string;
    public localState = { value: '' };
    @Output() productPivot : EventEmitter<any> = new EventEmitter();
    public showFilterBar:boolean=false;
    public showTitleBar:boolean =false;
    public titleHeading:any;
    public pageName:any;

    constructor(
      // public appState: AppState,
      public contentService : ContentService,
      public router: Router,
    ) {}
  
    public ngOnInit() {
      this.industry = localStorage.getItem("Industry");
      console.log('hello `feedbar` component');
      var urlBreakup = this.router.url.split('/');
      console.log(urlBreakup);
      this.pageName= urlBreakup[1];
      this.index = urlBreakup[1];
      this.contentType=urlBreakup[3];
     
     if(this.index=='like')
     this.titleHeading='Liked Posts';
     else if(this.index=='boutique')
     this.titleHeading='My Boutique';
     else if(this.index=='bookmark')
     this.titleHeading='Bookmarks';
     else if(this.index=='orders')
     this.titleHeading='My Orders';
     else if(this.index=='trending')
     this.titleHeading='Trending';
     else
     this.titleHeading='My Feed';


    
    }
    showMenu(){
    this.showFilterBar=false
    this.showTitleBar=!this.showTitleBar;
    }
    showFilter(){
    this.showTitleBar=false;
    this.showFilterBar=!this.showFilterBar;
    }
    changeHeading(type){
   this.titleHeading=type;
   this.showTitleBar=!this.showTitleBar;
    }
    goToContent(content){
      this.router.navigateByUrl('/dummy', {skipLocationChange: true}).then(()=>
      this.router.navigate(['feed/' + this.industry+'/'+content]));

    }
  
    public submitState(value: string) {
      console.log('submitState', value);
      // this.appState.set('value', value);
      this.localState.value = '';
    }
  }
  