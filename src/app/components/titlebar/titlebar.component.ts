import { Component, OnInit, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
// import { AppState } from '../../app.service';
import { SearchService } from "../../models/search/search.service";
import { ProductService } from "../../models/product/product.service";
import { appStoreLinks } from "../../models/globals";
import { AuthService } from "angular4-social-login";
import {Location} from '@angular/common';
require('aws-sdk/dist/aws-sdk');  

declare var $:any;
@Component({
  
  selector: 'titlebar', 
  providers: [ ],
  styleUrls: [ './titlebar.component.css' ],
  templateUrl: './titlebar.component.html'
})
export class TitlebarComponent implements OnInit, OnChanges {
  catagari: string;
  loginType: string;
  itemsCount: any;
  disablePopup: boolean = false;
  showonlyHam: boolean = false;
  username: string;
  public localState = { value: '' };
    @Input() addedToBag;
    @Output() emitCartItems : EventEmitter<any> = new EventEmitter();
    appStoreLinks : any = appStoreLinks;
   show : boolean = false;
   shoppingbag : boolean = false;
   notification : boolean = false;
   get : boolean = false;
   testLogin = localStorage.getItem("isLoggedIn");
   isLoggedIn : boolean;
   industry : string;
   isFeedPage : boolean = false;
   pageTitle:any;
   isShowSearch : boolean = false;
   displaySearchResluts : boolean = false;
   searchText;
   cartItems;
   totalCost;
   searchResult;
   tempUser;
  // public $: any;
  //public jQuery: any;


  constructor(
    private _location: Location,
    // public appState: AppState,
    public router : Router,
    public searchService : SearchService,
    private productService : ProductService,
    private authService: AuthService,
  ) {
      if(localStorage.getItem("isLoggedIn") == "true") {
        this.isLoggedIn = true;
      } else {
        this.isLoggedIn = false;
      }
  }

  public ngOnInit() {
$(document).ready(function(){

   $('.block1').click(function(){
    $('.mob-menu-overly').show();
    $('#fs-menu.fs-menu').css({'transition':'none','display':'block','position':'fixed',
    'top':'0','left':'0','width':'85%','z-index':'1','background-color':'#fff','padding-top':'15px'});
  });
    
  $('.mob-menu-overly').click(function(){
    $(this).hide();
    $('#fs-menu.fs-menu').removeClass('in collapse').css('display','none');
  });

});
 

    console.log('hello `titlebar` component');
    if(!localStorage.getItem("Industry") || localStorage.getItem("Industry") == 'undefined') {
      localStorage.setItem("Industry","Tollywood");
    }
    if(localStorage.getItem("isLoggedIn")) {
      this.getCartProducts();
      this.username=localStorage.getItem('name');
    }
    this.industry = localStorage.getItem("Industry");
    var urlBreakup = this.router.url.split("/");
    this.catagari=urlBreakup[2];
    this.pageTitle=urlBreakup[1];
    if(urlBreakup[1] == 'feed') {
      this.isFeedPage = true;
    }
    this.getProfilePicture();
  }
  hideHamburger(){
    if(this.showonlyHam==false){
    this.show=!this.show;
    
  }
  if(this.show==true){
    this.showonlyHam=false;
  }
  }
  showHamburger(){
   this.showonlyHam=true;
   
  }

  getProfilePicture() {
    this.tempUser = {
      name : localStorage.getItem("name"),
      username : localStorage.getItem("username"),
      profilePic : localStorage.getItem("profilePic")?localStorage.getItem("profilePic"):"http://www.hjakober.ch/wp-content/uploads/nobody_m_1024x1024.jpg"
    }
  } 

  public ngOnChanges(changes) {
    console.log(changes);
    if(changes.addedToBag.currentValue) {
      this.getCartProducts();
    }
  }

  getCartProducts() {
    this.productService.getCartByUser(localStorage.getItem("username")).subscribe(
      res => {
        console.log(res);
        this.cartItems = res.Items;
        console.log(this.cartItems);
        this.emitCartItems.emit(this.cartItems);
        this.totalCost = 0;
        for(let i=0; i<this.cartItems.length; i++) {
          this.totalCost = this.totalCost + Number(this.cartItems[i].productDetails.price*this.cartItems[i].productDetails.quantity);
        }
      },
      error => {
        console.log("error getting cart items");
      }
    );
  }

  placeOrder() {
    if(this.cartItems.length >0){
    let productArray = new Array();

      for(let i=0; i<this.cartItems.length; i++) {
        productArray.push(this.cartItems[i].productDetails);
      }
      let order = {
        product : productArray,
        userId : localStorage.getItem("username"),
        userName :String( localStorage.getItem("name")),
        status : "INACTIVE"
      }
      console.log(order);
      this.Order(order);
  }
}
checkFunction(){
  if(this.cartItems)
  if(this.cartItems.length>0 && !this.shoppingbag){
    this.itemsCount=this.cartItems.length;
    return true;
}
  else
    return false;

}

deleteItem(i){
 
  
    console.log(i);
  this.deleteCartItem(this.cartItems[i].id,this.cartItems[i],i);

  console.log(i);
}

deleteCartItem(id,body,i){
  this.cartItems.splice(i,1);
  this.productService.deleteItem(id,body).subscribe(
    res => {
        console.log(res);
        this.getCartProducts();
        
      },
      error => {
        console.log("Error in deleting orders");
      }
  );

}

  Order(body){
    this.productService.postOrder(body).subscribe(
      res => {
          console.log(res);
          console.log(res.id);
          this.router.navigate(['checkout/'+res.id]);
        },
        error => {
          console.log("Error in posting orders");
        }
    );
  }
  

  public showselected(){
    this.notification=false;
    this.shoppingbag =false;
    this.show = !this.show;
  }
  public shopbag(){
    this.notification=false;
    this.shoppingbag =!this.shoppingbag;
  }
  public notify(){
    this.shoppingbag =false;
    this.notification =!this.notification;
  }

  public getapp(){
    this.notification=false;
    this.shoppingbag =false;
    if(this.disablePopup==false){
    this.get = !this.get;
    }
    this.disablePopup=false;

  }
  disableApp(){
    this.disablePopup=true;
  }


  search(){
      console.log(this.searchText);
      if(this.searchText.length>2)
      if(this.searchText != "" && this.searchText != " ") {
        this.searchService.globalSearch(this.searchText)
        .subscribe(res => {
            console.log(res);
            this.searchResult = res;
            if(this.searchText == "" && this.searchText == " ")
            this.searchResult = false;
          },
          error => {
            console.log("Error in search");
          }
        );
      } else {
        this.searchResult = false;
      }
  }

  public gotofeed(first,second){  
    this.industry=second;
    localStorage.setItem("Industry",second);
    this.router.navigateByUrl('/dummy', {skipLocationChange: true}).then(()=>
    this.router.navigate([first,second]));
    // this.router.navigate([first,second]);

  }

  goToPage(string) {
    window.open(string)
  }

  goToPageFromSearch(first,second) {
      this.router.navigateByUrl('/dummy', {skipLocationChange: true}).then(()=>
      this.router.navigate([first,second]));
  }

  logout() {
    localStorage.setItem("isLoggedIn","false");
    localStorage.removeItem("name");
    localStorage.removeItem("username");
    localStorage.removeItem("token");
    localStorage.removeItem("userObj");
    localStorage.removeItem("profilePic");
  this.showonlyHam=false;
  this.router.navigate(['home']);
    this.authService.signOut();
  }

  toggleLogin () {
    console.log("in toggle login");
    if(localStorage.getItem("isLoggedIn") == "true") {
        this.testLogin = "false";
        localStorage.setItem("isLoggedIn","false"); 
    } else {
        this.testLogin = "true";
        localStorage.setItem("isLoggedIn","true"); 
    }
  }

  public displayLoginPopup : boolean = false;
  goToLogin(type) {
    if(type=='login'){
      this.loginType='login';
      }
      else
      this.loginType='signup';
    this.show = false;
    this.displayLoginPopup = true;
  }

  closeLoginPopup () {
    console.log("event emmitted");
    this.displayLoginPopup = false;
  }

  changePhoto(fileInput: any) {
      let AWSservice = (<any>window).AWS;
      AWSservice.config.accessKeyId = 'AKIAJT7SFJQEJTU5AOKA';
      AWSservice.config.secretAccessKey = '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';
      let bucket = new AWSservice.S3({params: {Bucket : 'bucket-for-image-test'}});
      console.log(fileInput.target.files);
      for(let i=0; i<fileInput.target.files.length; i++) {
         let file = fileInput.target.files[i];
         let params = {Key : file.name, Body : file};
         var _self = this;
         bucket.upload(params, function(err, res){
           console.log('error', err);
           console.log('response', res);
           if(res) {
             _self.tempUser.profilePic = res.Location;
           }
         })
      }
  }

  public submitState(value: string) {
    console.log('submitState', value);
    // this.appState.set('value', value);
    this.localState.value = '';
  }

  getTopMovies() {
    this.searchService.getSearchBySlug("search").subscribe(
     res => {
       console.log(res);
       // if(res) {

       // }

     },
     error => {
       console.log("error getting search");
     }
   );

  //  this.topMovies = this.topMoviesMock;
      // this.search = this.searchMock;
 }
 goBack(){
  this._location.back();
 }

 showSearch() {
  this.isShowSearch = !this.isShowSearch;
  this.displaySearchResluts = !this.displaySearchResluts;
 }
 checkMenu(type){
  if(type=='designer-store' ||type=='see-all' || type=='product' || type=='looks' || type=='content' || type=='celeb' || type=='movie'|| type=='celeb-store' || type=='movie-store' || type=='privacy' || type=='help' || type=='careers' || type=='terms'|| type=='about' || type=='return-policy' || type=='business' || type=='contact' || (type=='womenfashion' && this.catagari) || (type=='menfashion' && this.catagari))
    return false;
  else
    return true;
 }
 checkBack(type){
   if(type=='designer-store' || type=='see-all' || type=='product' || type=='looks'|| type=='content' || type=='celeb' || type=='movie' || type=='celeb-store' || type=='movie-store' || type=='privacy' || type=='help' || type=='careers' || type=='terms'|| type=='about' || type=='return-policy' || type=='business' || type=='contact' || (type=='womenfashion' && this.catagari) || (type=='menfashion' && this.catagari))
    return true;
  else
    return false;
 }
 disable(){
  this.searchResult = null;
  this.displaySearchResluts = false;
  this.notification=false;
  this.isShowSearch = false;
  this.shoppingbag =false;

 }
 

 focusOutFunction() {
   console.log("focus lost");
    this.isShowSearch = false;
    this.displaySearchResluts = false;
    this.searchResult = null;
    this.searchText = "";
 }
 
 goToLink(type) {
   var url;
   if(type == "facebook") {
    url = "http://facebook.com";
   } else if(type == "instagram") {
    url = "http://facebook.com";
   } else if(type == "twitter") {
    url = "http://facebook.com";
   } else if(type == "linkdin") {
    url = "http://facebook.com";
   }
   window.open(url,"_blank");
 }



}
