import {
    Component,
    OnInit,
    Input
  } from '@angular/core';
  import { Router } from '@angular/router';
  import { LikeService } from '../../models/like/like.service';

  
  // import { AppState } from '../../app.service';
  
  @Component({
    
    selector: 'cast-crew', 
    providers: [ ],
    styleUrls: [ './cast-crew.component.css' ],
    templateUrl: './cast-crew.component.html'
  })
  export class castcrewComponent implements OnInit {
    
    followCrewArray=[];
    followArray=[];
    loginPopup: boolean=false;
    @Input() cast : any;
    @Input() crew : any;
    @Input() SuggestedMovies :any;
    @Input() checkFollowing :any;
    flimShow:number=5;

    public localState = { value: '' };
  
    constructor(
      public router : Router,
      public likeService: LikeService,
      // public appState: AppState,
    ) {}
  
    public ngOnInit() {
      console.log('hello `cast-crew` component');
      if(this.cast)
      console.log(this.cast);
      if(this.checkFollowing)
     console.log(this.checkFollowing);
      
    }
    changeFilmShow(){
      if(this.flimShow==5){
        this.flimShow=10;
      }
      else{
        this.flimShow=5;
      }
    }
    goToMovies(slug){
      this.router.navigateByUrl('/dummy', {skipLocationChange: true}).then(()=>
      this.router.navigate(['movie/' + slug]));

    }
  
    public submitState(value: string) {
      console.log('submitState', value);
      // this.appState.set('value', value);
      this.localState.value = '';
    }
    followCeleb(celeb,i,followArr){
      var logged=localStorage.getItem('isLoggedIn');
      if(logged=='true'){
        if(followArr.data.Count=='0'){
          followArr.data.Count='1';
        
        }
        else{
          followArr.data.Count='0';
        }
      
  console.log(celeb+i);
  var profileDataObj = {
    id : celeb.id,
    name : celeb.name,
    slug : celeb.slug,
    profilePic : celeb.profilePic,
    type : 'celeb'
  };
  let followBody = {
    userId : localStorage.getItem("username"),
    entityId : celeb.id,
    entityObj :profileDataObj,
    type : "follow"
  };
  this.likeService.like(followBody).subscribe(
      res => {
          console.log(res);
        },
        error => {
          console.log("Error in posting follow");
        }
  );
  
  }
  else
  this.loginPopup=true;
    }
    closeLoginPopup () {
      console.log("event emmitted");
      this.loginPopup = false; 
    }
  }
  