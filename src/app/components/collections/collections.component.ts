import {
  Component,
  OnInit,
  Input
} from '@angular/core';

//import { AppState } from '../../app.service';
import { Router } from "@angular/router";
import { FashionService } from "../../models/fashion/fashion.service";
import { CarouselService } from '../../models/carousel/carousel.service';

@Component({

  selector: 'collections',
  providers: [],
  styleUrls: ['./collections.component.css'],
  templateUrl: './collections.component.html'
})
export class CollectionsComponent implements OnInit {
  menCatagorys: any;
  womenCatagarys: any;
  womencatagoriArray = [];
  mencatagoriArray = [];
  categories: any;
  subitems: string[];
  subItem: string;
  showSubCata: string;
  displayImages: any;
  catagory: string;
  content: string;
  string: any;
  inactiveArray = new Array();
  inactiveArrays = new Array();
  activeArray = new Array();
  public localState = { value: '' };
  @Input() nocarosel: any;
  
  @Input() carosel: any;
  @Input() shareYourStyleData: any;
  @Input() data: any;
  @Input() datas: any;

  public subPage;
  public items: string[];
  public item;
  public womenCollections;
  public menCollection;
  public shareYourStyle;
  public miniCard: boolean = true;
  public womenCatImages = [
    "https://bucket-for-image-test.s3.amazonaws.com/women-clothing.png",
    "https://bucket-for-image-test.s3.amazonaws.com/footwear.png",
    "https://bucket-for-image-test.s3.amazonaws.com/women-accessories.png",
    "https://bucket-for-image-test.s3.amazonaws.com/jewellery.png"
  ];
  public menCatImages = [
    "https://bucket-for-image-test.s3.amazonaws.com/men-clothing.png",
    "https://bucket-for-image-test.s3.amazonaws.com/menfootwear.png",
    "https://bucket-for-image-test.s3.amazonaws.com/men-accesories.png",
    "https://bucket-for-image-test.s3.amazonaws.com/ethinc-colection.png"
  ];
  public womenCatFootwear = [
    "https://bucket-for-image-test.s3.amazonaws.com/flats.png",
    "https://bucket-for-image-test.s3.amazonaws.com/Sandals.png",
    "https://bucket-for-image-test.s3.amazonaws.com/Bellies.png",
    "https://bucket-for-image-test.s3.amazonaws.com/boots.png",
    "https://bucket-for-image-test.s3.amazonaws.com/heels.png",
    "https://bucket-for-image-test.s3.amazonaws.com/Wedges.png",
    "https://bucket-for-image-test.s3.amazonaws.com/Stilettos.png",
    "https://bucket-for-image-test.s3.amazonaws.com/peep%20toe.png",
    "https://bucket-for-image-test.s3.amazonaws.com/flip%20flops.png",
    "https://bucket-for-image-test.s3.amazonaws.com/Sportsshoes.png",
    "https://bucket-for-image-test.s3.amazonaws.com/Sneakers.png",
    "https://bucket-for-image-test.s3.amazonaws.com/loafers.png",
  ];
  public menCatFootwear = [
    "https://bucket-for-image-test.s3.amazonaws.com/casuals.jpg",
    "https://bucket-for-image-test.s3.amazonaws.com/sports.jpg",
    "https://bucket-for-image-test.s3.amazonaws.com/sneaks.jpg",
    "https://bucket-for-image-test.s3.amazonaws.com/boots.jpg",
    "https://bucket-for-image-test.s3.amazonaws.com/loafers.jpg",
    "https://bucket-for-image-test.s3.amazonaws.com/formals.jpg",
    "https://bucket-for-image-test.s3.amazonaws.com/hiking.jpg",
    "https://bucket-for-image-test.s3.amazonaws.com/floaters.jpg",
    "https://bucket-for-image-test.s3.amazonaws.com/flip_flops.jpg",
   
  ];
  public womenCatJewellary = [
    "https://bucket-for-image-test.s3.amazonaws.com/BodY%20Chains.png",
    "https://bucket-for-image-test.s3.amazonaws.com/Necklaces.png",
    "https://bucket-for-image-test.s3.amazonaws.com/Bangles.png",
    "https://bucket-for-image-test.s3.amazonaws.com/Rings.png",
    "https://bucket-for-image-test.s3.amazonaws.com/Pendants.png",
    "https://bucket-for-image-test.s3.amazonaws.com/Treditional%20Jewellwey.png",
    "https://bucket-for-image-test.s3.amazonaws.com/Anklets.png",
    "https://bucket-for-image-test.s3.amazonaws.com/Bracelet.png",
    "https://bucket-for-image-test.s3.amazonaws.com/Brooches.png",
];
public menClothing = [
  "https://bucket-for-image-test.s3.amazonaws.com/polos___tees.jpg",
  "https://bucket-for-image-test.s3.amazonaws.com/casual_shirts.jpg",
  "https://bucket-for-image-test.s3.amazonaws.com/formal_trousers.jpg",
  "https://bucket-for-image-test.s3.amazonaws.com/jeans.jpg",
  "https://bucket-for-image-test.s3.amazonaws.com/casual_trousers.jpg",
  "https://bucket-for-image-test.s3.amazonaws.com/formal_shirts.jpg",
  "https://bucket-for-image-test.s3.amazonaws.com/suits_blazers.jpg",
  "https://bucket-for-image-test.s3.amazonaws.com/track_wear.jpg",
  "https://bucket-for-image-test.s3.amazonaws.com/shorts_.jpg",
  "https://bucket-for-image-test.s3.amazonaws.com/ethnic_wear_.jpg",
  "https://bucket-for-image-test.s3.amazonaws.com/winter_wear.jpg",
];
public menCatEyewear = [
  "https://bucket-for-image-test.s3.amazonaws.com/eyeglasses.jpg",
  "https://bucket-for-image-test.s3.amazonaws.com/sunglasses.jpg",
  
 ];
 public menCatBags = [
  "https://bucket-for-image-test.s3.amazonaws.com/backpacks.jpg",
  "https://bucket-for-image-test.s3.amazonaws.com/wallets.jpg",
  "https://bucket-for-image-test.s3.amazonaws.com/lap_top_bags.jpg",
  "https://bucket-for-image-test.s3.amazonaws.com/travel_bags.jpg",
  "https://bucket-for-image-test.s3.amazonaws.com/messenger_bags.jpg",
  
  
 ];
 public menCatWatches = [
  "https://bucket-for-image-test.s3.amazonaws.com/analog_watch.jpg",
  "https://bucket-for-image-test.s3.amazonaws.com/chronograph_watch.jpg",
  "https://bucket-for-image-test.s3.amazonaws.com/digital_watch.jpg",
  "https://bucket-for-image-test.s3.amazonaws.com/smart_watch.jpg",
  
  
 ];
  public menCatJewellary = [
    "https://bucket-for-image-test.s3.amazonaws.com/chains.jpg",
    "https://bucket-for-image-test.s3.amazonaws.com/rings.jpg",
    "https://bucket-for-image-test.s3.amazonaws.com/bracelets.jpg",
   ];
  public womenEthnicWear=[
    "https://bucket-for-image-test.s3.amazonaws.com/Kurtas%20%26%20Kurtis.png",
    "https://bucket-for-image-test.s3.amazonaws.com/Suits%20Sets.png",
    "https://bucket-for-image-test.s3.amazonaws.com/Sarees%20%26%20Dress%20Material.png",
    "https://bucket-for-image-test.s3.amazonaws.com/Salwars%20%26%20Churidars.png",

  ];
  public womenFusionWear=[
    "https://bucket-for-image-test.s3.amazonaws.com/Kurtas%20%26%20Suits.png",
    "https://bucket-for-image-test.s3.amazonaws.com/Kurtis.png",
    "https://bucket-for-image-test.s3.amazonaws.com/Tunics%20%26%20Tops.png",
    "https://bucket-for-image-test.s3.amazonaws.com/Skirts%20%26%20Palazzos.png",
    "https://bucket-for-image-test.s3.amazonaws.com/Sarees%20%26%20Blouses.png",
    "https://bucket-for-image-test.s3.amazonaws.com/Dupattas%20%26%20Shawls.png",
    "https://bucket-for-image-test.s3.amazonaws.com/Jackets.png",
    "https://bucket-for-image-test.s3.amazonaws.com/Waistcoats.png",

  ];
  public womenWesternWear = [
    "https://bucket-for-image-test.s3.amazonaws.com/Tops.png",
    "https://bucket-for-image-test.s3.amazonaws.com/Tees%20and%20Shirts.png",
    "https://bucket-for-image-test.s3.amazonaws.com/Dress%20%26%20jumpsuits.png",
    "https://bucket-for-image-test.s3.amazonaws.com/Trousers%20%26%20Jeans.png",
    "https://bucket-for-image-test.s3.amazonaws.com/Leggings%20%26%20Jeggings.png",
    "https://bucket-for-image-test.s3.amazonaws.com/Capris.png",
    "https://bucket-for-image-test.s3.amazonaws.com/Shorts%20%26%20Skirts.png",
    "https://bucket-for-image-test.s3.amazonaws.com/Winter%20Jackets.png",
    "https://bucket-for-image-test.s3.amazonaws.com/Clothing%20Accessories.png",
    "https://bucket-for-image-test.s3.amazonaws.com/Sweaters.png",
    "https://bucket-for-image-test.s3.amazonaws.com/Winter%20Shrugs.png",
    "https://bucket-for-image-test.s3.amazonaws.com/Sweatshirts.png",
    
  ];
  public womenCatAccessories = [
    "https://bucket-for-image-test.s3.amazonaws.com/Handbags.png",
    "https://bucket-for-image-test.s3.amazonaws.com/sling%20bags.png",
    "https://bucket-for-image-test.s3.amazonaws.com/clutches.png",
    "https://bucket-for-image-test.s3.amazonaws.com/Eye%20Glasses.png",
    "https://bucket-for-image-test.s3.amazonaws.com/Totes.png",
    "https://bucket-for-image-test.s3.amazonaws.com/Wallets.png",
    "https://bucket-for-image-test.s3.amazonaws.com/sunglasses.png",
    
  ];
  public menCatAccessories = [
    "https://bucket-for-image-test.s3.amazonaws.com/bags.jpg",
    "https://bucket-for-image-test.s3.amazonaws.com/belts.jpg",
    "https://bucket-for-image-test.s3.amazonaws.com/caps___hats.jpg",
    "https://bucket-for-image-test.s3.amazonaws.com/jewellery.jpg",
    "https://bucket-for-image-test.s3.amazonaws.com/scarves___mufflers.jpg",
    "https://bucket-for-image-test.s3.amazonaws.com/tech_accessories.jpg",
    "https://bucket-for-image-test.s3.amazonaws.com/tie___cuffs.jpg",
    "https://bucket-for-image-test.s3.amazonaws.com/watches_2.jpg",
    
  ];
  constructor(
   // public appState: AppState,
    public router: Router,
    public carouselService: CarouselService,
    public fashionService: FashionService
  ) { }

  public ngOnInit() {
    if (this.data) {
      console.log(this.data);
      this.womenCollections = this.data;
      this.getCarouselArray(this.data, 3);


    }
    this.getcategories();
    if (this.nocarosel)
      console.log(this.nocarosel);
    var urlBreakup = this.router.url.split("/");
    this.content = urlBreakup[1];
    this.showSubCata = urlBreakup[3];


    if (this.datas) {
      this.menCollection = this.datas;
      this.getCarouselArray(this.datas, 3);
    }

    if (this.shareYourStyleData) {
      this.shareYourStyle = this.shareYourStyleData;
      this.getCarouselArray(this.shareYourStyle, 3);
    }
    this.catagory = localStorage.getItem('subPage');
    var subCatagori=localStorage.getItem('subCatagori');
    if (this.catagory == 'Footwear' && this.content=='womenfashion') {
      this.displayImages = this.womenCatFootwear;
    }
    if (this.catagory == 'Footwear' && this.content=='menfashion') {
      
      this.displayImages = this.menCatFootwear;
    }
    if (this.catagory == 'Jewellery' && this.content=='womenfashion') {
      this.displayImages = this.womenCatJewellary;
    }
    if (this.catagory == 'Jewellery' && this.content=='menfashion') {
      this.displayImages = this.menCatJewellary;
    }
    if (this.catagory == 'Ethnic Wear' && subCatagori=='Clothing') {
      this.displayImages = this.womenEthnicWear; 
    }
    if (this.catagory == 'Fusion Wear' && subCatagori=='Clothing') {
      this.displayImages = this.womenFusionWear; 
    }
    if (this.catagory == 'Clothing' && this.content=='menfashion') {
      this.displayImages = this.menClothing; 
    }
    if (!this.catagory  && subCatagori=='Clothing') {
      this.displayImages = []; 
    }
    if (this.catagory == 'Westren Wear' && subCatagori=='Clothing') {
      this.displayImages = this.womenWesternWear; 
    }
    if (subCatagori == 'Accessories' && !this.catagory && this.content=='womenfashion') {
      this.displayImages = this.womenCatAccessories;
    }
    if (subCatagori == 'Accessories' && !this.catagory && this.content=='menfashion') {
      this.displayImages = this.menCatAccessories;
    }
    if (subCatagori == 'Eyewear' && !this.catagory && this.content=='menfashion') {
      this.displayImages = this.menCatEyewear;
    }
    if (subCatagori == 'Accessories' && this.catagory=='Bags' && this.content=='menfashion') {
      this.displayImages = this.menCatBags;
    }
    if (subCatagori == 'Accessories' && this.catagory=='Watches' && this.content=='menfashion') {
      this.displayImages = this.menCatWatches;
    }
    console.log('hello `collections` component');

    this.subPage = localStorage.getItem("subPage");
    if (this.content == 'womenfashion' || 'menfashion') {
      this.subItem = localStorage.getItem('subItem');
      this.item = localStorage.getItem('items');
      if (this.item) {
        console.log("************DATA ITEMS**********");
        console.log(this.item);
        this.items = this.item.toString().split(",");
        console.log(this.items);
      }
      if (this.subItem) {
        console.log("************DATA ITEMS**********");
        console.log(this.subItem);
        this.subitems = this.subItem.toString().split(",");
        console.log(this.subitems);
      }


    }

  }
  goToSeeAll(page, subPage, category, ) {
    localStorage.setItem("subPage", subPage);
    localStorage.setItem("page", page);
    localStorage.setItem("category", category);

    this.router.navigate([page, subPage, category]);
  }
  goToSubpage(subitem, item) {
    this.getcategories();
    console.log(item);
    if (this.content == 'menfashion') {
      if (subitem == 'Accessories') {



        if (item == 'Bags')
          localStorage.setItem('items', 'Bags,Backpacks,Wallets,Laptop Bags,Travel Bags,Messanger Bags');
        if (item == 'Jewellery')
          localStorage.setItem('items', 'Jewellery,Chains,Rings,Bracelets');
        if (item == 'Watches')
          localStorage.setItem('items', 'Watches,Analog,Chronograph,Digital,smart');
        if (item == 'Ties & Cuf')
          localStorage.setItem('items', 'Ties & Cuf');
        if (item == 'Tech Accessories')
          localStorage.setItem('items', 'Tech Accessories');
        if (item == 'Mufflers & Scarves')
          localStorage.setItem('items', 'Mufflers & Scarves');
        if (item == 'Caps & Hats')
          localStorage.setItem('items', 'Caps & Hats');
        if (item == 'Belts')
          localStorage.setItem('items', 'Belts');

      }
      if (subitem == 'Eyewear') {
        if (item == 'Sunglasses')
          localStorage.setItem('items', 'Sunglasses,Aviators,Wayfarer');
        if (item == 'Eyeglasses')
          localStorage.setItem('items', 'Eyeglasses');

      }
    }
    if (this.content == 'womenfashion') {
      if (subitem == 'Clothing') {
        if (item == 'Ethnic Wear')
          localStorage.setItem('items', 'Ethnic Wear,Kurtas & Kurtis,Suits Sets,Sarees & Dress Material,Salwars & Churidars');
        if (item == 'Fusion Wear')
          localStorage.setItem('items', 'Fusion Wear,Kurtas & Suits,Kurtis, Tunics & Tops,Skirts & Palazzos,Sarees & Blouses,Dupattas & Shawls,Jackets,Waistcoats & Capes');
        if (item == 'Westren Wear')
          localStorage.setItem('items', 'Westren Wear,Tops , Tees & Shirts,Dresses & Jumpsuits,Trousers & Jeans,Leggings & Jeggings,Capris,Shorts & Skirts,Winter Jackets,Clothing Accessories,Sweaters,Winter Shrugs,Sweatshirts');
      }
      if (subitem == 'Accessories') {
        if (item == 'Bags')
          localStorage.setItem('items', 'Bags,Hand Bags,Sling Bags,Clutches,Totes,Wallets');
        if (item == 'Belts')
          localStorage.setItem('items', 'Belts');
        if (item == 'Eye Wear')
          localStorage.setItem('items', 'Eye Wear,Sun Galsses,Eye Galsses');
        if (item == 'Caps & Hats')
          localStorage.setItem('items', 'Caps & Hats');
        if (item == 'Hair Accessories')
          localStorage.setItem('items', 'Hair Accessories');
        if (item == 'phone Cases')
          localStorage.setItem('items', 'phone Cases');
        if (item == 'Scarves Stoles & Gloves')
          localStorage.setItem('items', 'Scarves Stoles & Gloves');
        if (item == 'Travel Accessories')
          localStorage.setItem('items', 'Travel Accessories');
        if (item == 'Watches')
          localStorage.setItem('items', 'Watches');

      }

    }
    localStorage.setItem("subPage", item);

    this.router.navigate([this.content, subitem, item]);
  }
  goToProduct(subitem, item) {
    localStorage.setItem('category', item);
    var cat = localStorage.getItem('subCatagori');
    this.router.navigate([this.content, cat, subitem, item]);

  }
  getcategories() {
    this.fashionService.getCategories().subscribe(
      res => {
        console.log("THIS IS Categories DATA");
        console.log(res);
        if (res) {
          this.categories = res.Items[0];
          console.log(Object.keys(this.categories.menFashion[0]));
          this.menCatagorys = this.categories.menFashion;
          this.womenCatagarys = this.categories.womenFashion;
          for (let i = 0; i < this.categories.menFashion.length; i++) {
            this.mencatagoriArray.push(Object.keys(this.categories.menFashion[i])[0]);
          }
          for (let i = 0; i < this.categories.womenFashion.length; i++) {
            this.womencatagoriArray.push(Object.keys(this.categories.womenFashion[i])[0]);
          }
          console.log(this.mencatagoriArray);
          console.log(this.womencatagoriArray);
        }
      },
      error => {
        console.log("Error in getting categories");
      }
    );
  }

  goToWomenCatogery(page, subPage, i) { 
    console.log(subPage);
    if (subPage == 'Clothing') {
      localStorage.setItem('items', 'Clothing,Ethnic Wear,Fusion Wear,Westren Wear');
      localStorage.setItem('subItem', 'Clothing,Ethnic Wear,Fusion Wear,Westren Wear');
      localStorage.setItem('subCatagori', 'Clothing');
      localStorage.removeItem('subPage');
    }
    if (subPage == 'Accessories') {
      localStorage.setItem('items', 'Accessories,Bags,Belts,Caps & Hats,Eye Wear,Hair Accessories,phone Cases,Scarves Stoles & Gloves,Travel Accessories,Watches');
      localStorage.setItem('subItem', 'Accessories,Bags,Belts,Caps & Hats,Eye Wear,Hair Accessories,phone Cases,Scarves Stoles & Gloves,Travel Accessories,Watches');

      localStorage.setItem('subCatagori', 'Accessories');
      localStorage.removeItem('subPage');
    }
    if (subPage == 'Footwear') {
      localStorage.setItem('items', 'Footwear,Flats,Sandals,Bellies,Boots,Heels,Wedges,Stilettos,Peep Toes,Flip Flops,Sports Shoes,Sneakers,Loafers');

      localStorage.setItem('subPage', 'Footwear');
      localStorage.removeItem('subCatagori');
    }
    if (subPage == 'Jewellery') {
      localStorage.setItem('items', 'Jewellery,Earrings,Necklaces & Necklace Sets,Bangles & Bracelets,Rings & Bands,Pendants & Pendant Sets,Traditional Jewellery,Anklets,Brooches,Body Chains');

      localStorage.setItem('subPage', 'Jewellery');
      localStorage.removeItem('subCatagori');
    }

    localStorage.removeItem('category');

    localStorage.setItem("rootRoute", 'womenfashion');


    this.router.navigate([page, subPage]);
  }
  goToMenCatogery(page, subPage, i) {
    console.log(page);
    if (subPage == 'Clothing') {
      localStorage.setItem('items', 'Clothing,Polos & Tees,Casual Shirts,Casual Shirts,Jeans,Casual Trousers,Formal Shirts,Suits & Blazers,Track Wear,Shorts & 3/4ths,Ethnic Wear,Winter Wear');

      localStorage.setItem('subPage', 'Clothing');
      localStorage.removeItem('subCatagori');
    }
    if (subPage == 'Footwear') {
      localStorage.setItem('items', 'Footwear,Casual Shoes,Sports Shoes,Sneakers,Boots,Loafers,Formal Shoes,Outdoor & Hiking,Floaters,Flip Flop');

      localStorage.setItem('subPage', 'Footwear');
      localStorage.removeItem('subCatagori');
    }
    if (subPage == 'Eyewear') {
      localStorage.setItem('items', 'Eyewear,Eyeglasses,Sunglasses');
      localStorage.setItem('subItem', 'Eyewear,Eyeglasses,Sunglasses');
      localStorage.setItem('subCatagori', 'Eyewear');
      localStorage.removeItem('subPage');
    }
    if (subPage == 'Accessories') {
      localStorage.setItem('items', 'Accessories,Bags,Belts,Caps & Hats,Jewellery,Mufflers & Scarves,Tech Accessories,Ties & Cuf,Watches');
      localStorage.setItem('subItem', 'Accessories,Bags,Belts,Caps & Hats,Jewellery,Mufflers & Scarves,Tech Accessories,Ties & Cuf,Watches');
      localStorage.setItem('subCatagori', 'Eyewear');
      localStorage.removeItem('subPage');
    }

    localStorage.removeItem('category');

    localStorage.setItem("rootRoute", 'menfashion');



    this.router.navigate([page, subPage]);
  }

  getCarouselArray(data, num) {
    console.log(data);

    for (let i = 0; i < num; i++) {
      if (data[i]) {
        this.activeArray.push(data[i][0]);
      }
    }
    if (data.length > num) {
      var inactiveArrayCount = 0;
      for (let i = num; i < data.length; i++) {
        if (i % num == 0) {
          this.inactiveArrays.push(new Array());
          inactiveArrayCount++;
        }
        this.inactiveArrays[Number(inactiveArrayCount - 1)].push(data[i][0]);
      }
    }
    console.log(this.activeArray);
    console.log(this.inactiveArrays);
    this.inactiveArray = this.inactiveArrays[0];
  }
  // displayImg(){
  //   var picType =localStorage.getItem('subPage');
  //   if(picType=='Footwear'){
  //     this.displayImages=this.womenCatFootwear;
  //     return true;
  //   }
  //   return false;
  // }

  public submitState(value: string) {
    console.log('submitState', value);
   // this.appState.set('value', value);
    this.localState.value = '';
  }


}
