import {
  Component,
  OnInit, Input
} from '@angular/core';
import { ShareButtons } from '@ngx-share/core';

// import { AppState } from '../../app.service';
import { LikeService } from '../../models/like/like.service';
import { Router } from '@angular/router';
import { EmailService } from '../../models/email/email.service';

@Component({
  
  selector: 'card-socialbar', 
  providers: [ ],
  styleUrls: [ './card-socialbar.component.css' ],
  templateUrl: './card-socialbar.component.html'
})
export class CardSocialbarComponent implements OnInit {
  imageUrl: any;
  public localState = { value: '' };
  public socialshare:boolean=false;
  public sendemail:boolean=false;
  public counter : number = 0;
  public isLike : boolean = false;
  public isBookmark : boolean = false;
  public display : boolean =false;
  public message: any;  
  public currentUrl : any;
  public sendEmailMessage : any;
  public sImage : any;
  public contentTitle : any;

  public contentUrl : any;
  // public imageUrl='../../../assets/img/card_share.png';
  @Input() shareSlug; 
  @Input() cardId;           
  @Input() productSlug;   
  @Input() shareProfilePic; 
  @Input() image;       
  @Input() contentData;       
  @Input() content;       

  constructor(
    // public appState: AppState,
    public likeService: LikeService,
    public emailService : EmailService,
    public router : Router,
    public sharee: ShareButtons,
  ) {}

  public ngOnInit() {
    // if(this.shareProfilePic){
    //   console.log(this.shareProfilePic);
    //   this.imageUrl=this.shareProfilePic;
    // }
    console.log(this.image);
    this.sImage=this.image;
    console.log('hello `card-socialbar` component');
    this.isLiked(this.cardId);
    if(localStorage.getItem("isLoggedIn") == "true") {
      console.log(this.cardId);
      this.isBookmarked(this.cardId);
    }
    if(this.contentData){
      console.log(this.contentData);
      this.contentTitle = this.contentData.title ? this.contentData.title : this.contentData.name;
      if(this.contentData.contentType=='gallery')
      this.imageUrl=this.contentData.media.gallery[0];
      else
      this.imageUrl=this.contentData.profilePic;
    }
    this.currentUrls();
    this.getContentUrl();
  }

  currentUrls(){
    this.currentUrl = this.router.url;
    console.log("**********CURENT URL************");
   console.log(this.currentUrl)
  }

  getContentUrl(){
    if(this.productSlug)
    this.contentUrl = "http://flikster.com/product/"+this.productSlug;
    else
    this.contentUrl = "http://flikster.com/content/"+this.shareSlug;
    
  }

  isLiked(id) { 
    let isLikeBody = {
      userId : localStorage.getItem("username")?localStorage.getItem("username"):"null",
      entityId : id,
      type : "like"
    };
    this.likeService.isLiked(isLikeBody).subscribe(
        res => {
            console.log(res);
            this.counter = res.totalCount;
            if(res.data) {
              if(res.data.Count == 0) {
                this.isLike = false;
              } else {
                this.isLike = true;
              }
            }
          },
          error => {
            console.log("Error");
          }
    );
  }

  isBookmarked(id) {
    let isLikeBody = {
      userId : localStorage.getItem("username"),
      entityId : id,
      type : "bookmark"
    };
    this.likeService.isLiked(isLikeBody).subscribe(
        res => {
            console.log(res);
            if(res.data) {
              if(res.data.Count == 0) {
                this.isBookmark = false;
              } else {
                this.isBookmark = true;  
              }
            }
          },
          error => {
            console.log("Error");
          }
    );
  }
 
  public share(){
    this.sendemail=false;
    this.socialshare= !this.socialshare;
    
  }
  disable(){
    this.sendemail=false;
    this.socialshare= false;
    this.display=false;

  }

  public send(){
    this.socialshare=false;
    this.sendemail= !this.sendemail;
  }

  like(){
    if(localStorage.getItem("isLoggedIn") == "true") {
      if(this.isLike) {
        this.counter--;
      } else {
        this.counter++;
      }
      this.isLike = !this.isLike;
      let likeBody = {
        userId : localStorage.getItem("username"),
        entityId : this.cardId,
        type : "like"
      };
      this.likeService.like(likeBody).subscribe(
          res => {
              console.log(res);
            },
            error => {
              console.log("Error in posting like");
            }
      );
    } else {
      //redirect to login
      // this.router.navigate(['login']); 
      this.displayLoginPopup = true; 
      console.log("rediret to login");
    }
  }

  bookmark(){
    if(localStorage.getItem("isLoggedIn") == "true") {
      this.isBookmark = !this.isBookmark;
      let bookmarkBody = {
        userId : localStorage.getItem("username"),
        entityId : this.cardId,
        type : "bookmark"
      };
      this.likeService.like(bookmarkBody).subscribe(
          res => {
              console.log(res);
            },
            error => {
              console.log("Error in posting bookmark");
            }
      );
    } else {
      //redirect to login
      // this.router.navigate(['login']); 
      this.displayLoginPopup = true; 
      console.log("rediret to login");
    }
  }

  displayUrl(){
    this.display = !this.display;
    this.sendemail=false;
    this.socialshare= !this.socialshare;

  }
  close(){
    this.display=!this.display;
  }

  public submitState(value: string) {
    console.log('submitState', value);
    // this.appState.set('value', value);
    this.localState.value = '';
  }
  shareCardViaEmail(){
    console.log(this.sendEmailMessage);
    var title;
    var text;
    if(this.content) {
       title = this.content.title? this.content.title : this.content.name;
       text = this.content.text? this.content.text : null;
    }

    let isSendEmailBody = {
      userId : localStorage.getItem("username"),
     toEmail : this.sendEmailMessage,
     title : title,
     text : text,
     url : "http://flikster.com/content/"+this.shareSlug,
     pic : this.shareProfilePic
     
    };

    this.emailService.shareCardViaEmail(isSendEmailBody).subscribe(
      
        res => {
            if(res) {
              
              if(res.statusCode == 200) {
                this.sendEmailMessage="";
                this.message = "shared Successfully";

              } else {
                console.log(res.message);

              }
            }
          },
          error => {
            console.log("Error");
          }
    );
}


  public displayLoginPopup : boolean = false;

  closeLoginPopup () {
    console.log("event emmitted");
    this.displayLoginPopup = false;
  }
}
