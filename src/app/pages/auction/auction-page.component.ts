import {
  Component,
  OnInit
} from '@angular/core';
import { Router } from '@angular/router';
declare var $:any;
// import { AppState } from '../../app.service';
import { ProductService } from "../..//models/product/product.service";
import { FashionService } from "../../models/fashion/fashion.service";
import { MetaService } from '@ngx-meta/core';

@Component({
  selector: 'auction',
  providers: [],
  styleUrls: ['./auction-page.component.css'],
  templateUrl: './auction-page.component.html'
})

export class AuctionPageComponent implements OnInit {
  completedarray=[];
  currBid: any;
  secondsLeft: number;
  public today: number = Date.now();
  public showhowitwork: boolean;
  public showcurrent: boolean;
  public showupcoming: boolean;
  public auctionstype: string;
  public localState = { value: '' };
  public pageType;
  public widgets;
  public currentData;
  public upcomingData;
  public currentarray = [];
  public icurrentarray = [];
  public upcomingarray = [];
  minutsLeft: number;
  daysLeft: number;
  hoursLeft: number;
  milisecondsDiff: number;
  miliseconds: number;
  endDate: any;
  startDate: any;
  private alive: boolean = true;

  constructor(
    // public appState: AppState,
    public router: Router,
    public productService: ProductService,
    public fashionService: FashionService,
    private metaService: MetaService,
  ) { }

  public ngOnInit() {
$(document).ready(function(){
    
  $('.Upcoming_Auction .media-carousel .item .thumbnail span').click(function(){
    $(this).toggleClass('active');
  });

});

    console.log('hello `auction` component');
    var urlBreakup = this.router.url.split("/");
    console.log(urlBreakup);
    this.pageType = urlBreakup[1];
    if (this.pageType) {
      this.metaService.setTitle('Online Auction In India For Celebrity Dresses | Online Bidding In India');
      this.metaService.setTag('keywords', 'celebrity dresses auctions, auctions in india, free auctions');
      this.metaService.setTag('og:title', 'Best Online Auction In India For Celebrity Dresses | Online Bidding In India');
      this.metaService.setTag('og:description', ' Flikster is the online shopping site in India. Here you can bid for free! Whether you are looking for Indian Celebrity dresses, Men Fashion, Women Fashion and accessories - Flikster has great deals on women�s and men�s latest fashion designer dresses, jewellery, footwear, watches, accessories and much more only on flikster. So lets bid and get a chance to win products.');
      this.metaService.setTag('og:keywords', 'celebrity dresses auctions, auctions in india, free auctions');
      this.metaService.setTag('fb:app_id', '433236906796403');
      this.metaService.setTag('og:type', 'article');
      this.metaService.setTag('og:url', 'http://flikster.com');
    }
    this.auctionstype = urlBreakup[2];
    if (!urlBreakup[2] || urlBreakup[2] == 'upcoming') {
      this.showupcoming = true;

    }
    if (!urlBreakup[2] || urlBreakup[2] == 'current') {
      this.showcurrent = true;

    }
    if (!urlBreakup[2]) {
      this.showhowitwork = true;

    }
    // this.getUpcomingAuctions("auctions","docsPerPage");
    // this.getCurrentAuctions("auctions","docsPerPage");
    window.scrollTo(0, 0);
    this.getCompletedAuctions();
    this.getCurrentAuctions("auctions", "docsPerPage");
    this.getCategoryWidgets("auctions");

  }
  getCurrentAuctions(auctions, docsPerPage) {
    this.productService.allAuctions(auctions, docsPerPage).subscribe(
      res => {
        console.log(res);
        if (res) {
          this.currentData = res;
          console.log("********AUCTIONS DATA*********");
          console.log(this.currentData);
          for (let i = 0; i < this.currentData.Items.length; i++) {
            if (this.checkdate(this.currentData.Items[i].startDate, this.today)) {
              this.icurrentarray.push(this.currentData.Items[i]);
             }
            else {

              this.upcomingarray.push(this.currentData.Items[i]);

            }

          }
          for (let i = 0; i < this.icurrentarray.length; i++) {
            if (this.checkdate(this.today, this.icurrentarray[i].endDate)) {

              this.currentarray.push(this.icurrentarray[i]);


            }
            


          }
          // console.log(this.completedarray);
        }

      },
      error => {
        console.log("error getting currentData");
      }
    );



  }
  getCompletedAuctions(){
    this.productService.getCompleted().subscribe(
      res => {
        console.log(res);
        if (res) {
          for(let i=0;i<res.hits.hits.length;i++){
            this.completedarray.push(res.hits.hits[i]._source);
          }
        }
        console.log(this.completedarray);
      });

  }
  
  getEndTime(item){
    console.log(item);
    
    this.startDate = new Date( item.endDate);
    this.endDate = this.getPresentTime();
    this.miliseconds = this.startDate - this.endDate;
    this.daysLeft=Math.floor(this.miliseconds / 1000 /60 /60 /24);
    this.hoursLeft= Math.floor(this.miliseconds / 1000 /60 /60-this.daysLeft*24); 
    this.minutsLeft= Math.floor(this.miliseconds / 1000 /60- this.hoursLeft*60-this.daysLeft*60*24 ); 
    this.secondsLeft=Math.floor(this.miliseconds / 1000 - this.hoursLeft*60*60-this.daysLeft*60*60*24-this.minutsLeft*60);
    console.log(this.milisecondsDiff);
    return true;
  }
  getStartTime(item){
    this.startDate = new Date( item.startDate);
    this.endDate = this.getPresentTime();
    this.miliseconds = this.startDate - this.endDate;
    this.daysLeft=Math.floor(this.miliseconds / 1000 /60 /60 /24);
    this.hoursLeft= Math.floor(this.miliseconds / 1000 /60 /60-this.daysLeft*24); 
    this.minutsLeft= Math.floor(this.miliseconds / 1000 /60- this.hoursLeft*60-this.daysLeft*60*24 ); 
    this.secondsLeft=Math.floor(this.miliseconds / 1000 - this.hoursLeft*60*60-this.daysLeft*60*60*24-this.minutsLeft*60);
    console.log(this.milisecondsDiff);
    return true;

  }
  getPresentTime(){
    return new Date(Date.now());
  }
  getCurrentBid(item){
    if(item.bids)
    if(!item.bids[0]){
    this.currBid=(+item.startingPrice)+(+item.bidIncrement);
    }
    else
    if(item.bids[0]){
    this.currBid=(+item.bids[item.bids.length-1].bidAmount)+(+item.bidIncrement);
  }

    return true;
  }

  getCategoryWidgets(tag) {
    this.fashionService.getCategoryWidgets(tag).subscribe(
      res => {
        if (res) {
          console.log(res);
          this.widgets = res.hits.hits;
          // this.contentType = this.contents.contentType;
          // this.getRelatedFliks(this.contentType);
          console.log("***********widgets**********")
          console.log(this.widgets);
        }
      },
      error => {
        console.log("error getting content");
      }
    );
  }
  //    getUpcomingAuctions(auctions,docsPerPage) {
  //     this.productService.UpcomingAuctions(auctions,docsPerPage).subscribe(
  //      res => {
  //        console.log(res);
  //         if(res) {
  //           this.upcomingData = res;

  //         }

  //      },
  //      error => {
  //        console.log("error getting upcomingData");
  //      }
  //    );



  //  }

  public checkdate(date1, date2) {
    console.log(date1);
    console.log(date2);
    return (new Date(date2) > new Date(date1));


  }
  

  public submitState(value: string) {
    console.log('submitState', value);
    // this.appState.set('value', value);
    this.localState.value = '';
  }
}
