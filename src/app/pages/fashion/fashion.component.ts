

import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
// import { AppState } from '../../app.service';
import { FashionService } from "../../models/fashion/fashion.service";
import { ContentService } from "../../models/content/content.service";
import { BrandService } from "../../models/brand/brand.service";
import { CarouselService } from "../../models/carousel/carousel.service";
import { MetaService } from '@ngx-meta/core';
import {DomSanitizer} from '@angular/platform-browser'
  @Component({
    
    selector: 'fashion', 
    providers: [ ],
    styleUrls: [ './fashion.component.css' ],
    templateUrl: './fashion.component.html'
  })
  export class FashionComponent implements OnInit, OnDestroy {
    auctionData=[];
    displayImage: any;
    shopByStar=[];
    bigScreensProducts=[];
    smallScreensProducts=[];
    latestShirtsProducts=[];
    videoUrl: any;
    latestDesignerProducts=[];
    AccessoriesProducts=[];
    activeFootwareArray=[];
    inactiveFootwareArrays=[];
    latestFootwareProducts=[];
    inactiveBrandDetailsArrays=[];
    activeBrandDetailsArray=[];
    brandDetailsArray=[];
    brandArray=[];
    inactiveClothingArrays=[];
    activeClothingArray=[];
    latestClothingProducts=[];
    latestArrivedProducts=[];
    categoryArray = [];
    allProducts=[];
    showAllProducts: boolean =false;
    priceMaxFilter: any;
    priceMinFilter: any;
    mencat: boolean;
    womencat: boolean;
    isDiscountedType: any;
    fashionType: any;
    tempArray: any;
    isDiscounted: boolean = false;
    productsArray: any;
    showType: any;
    showFilteredData: boolean = false;
    industry: string;
    pagetype: string;
    shopbyVideosData=[];
    carouselData: any;
    stealThatStyleCelebData: any;
    public localState = { value: '' };
    public pageType : string;
    public pagecontent : any;
    public stealThatStyle = "stealThatStyle";
    
    public videos = "shopbyvideos";
    public carouselType = "shopByVideos";
    public fourArray = [1,2,3,4];
    public stealThatStyleData;
    public bargainData =[];
    public bargainStyle = "bargainStyle";
    public contentType3 = "trailer";
    public topTrailers;
    public shareYourStyleData;
    public caurosel="caurosel";
    public isDesigner : boolean = false;
    public design = "design";
    public designerProducts;
    public widgets;
    public celebName: string;
    public activeArray = [];
    public inactiveArray = [];
    public inactiveArrays = [];
    public page;
    public storeType;
    public celebrity;
    public rootRoute;
    public subRoute;
    public seemore:number=3;
    public see : boolean = false;
    public brand;
    public designer;
    public designerMenProducts;
    public designerWomenProducts;
    public show=1; 
    public showno=['Sarees','Shirts','T-shirts','Coats'];
    public _minpriceOptions = [
      { "valueP": null },  
      { "valueP": 500 }, 
      { "valueP": 1000 }, 
      { "valueP": 2000 }, 
      { "valueP": 3000 }, 
      { "valueP": 4000 }, 
      { "valueP": 5000 }, 
      { "valueP": 10000 }, 
      { "valueP": 20000 }, 
      { "valueP": 30000 }, 
      { "valueP": 40000 }, 
      { "valueP": 50000 }, 
      { "valueP": 60000 }, 
      { "valueP": 70000 },
      { "valueP": 80000 }, 
      { "valueP": 90000 }, 
      { "valueP": 100000 }, 
      { "valueP": 150000 }, 
      { "valueP": 150000 }
    ];
    public _maxpriceOptions = [
      { "valueP": null },  
      { "valueP": 500 }, 
      { "valueP": 1000 }, 
      { "valueP": 2000 }, 
      { "valueP": 3000 }, 
      { "valueP": 4000 }, 
      { "valueP": 5000 }, 
      { "valueP": 10000 }, 
      { "valueP": 20000 }, 
      { "valueP": 30000 }, 
      { "valueP": 40000 }, 
      { "valueP": 50000 }, 
      { "valueP": 60000 }, 
      { "valueP": 70000 },
      { "valueP": 80000 }, 
      { "valueP": 90000 }, 
      { "valueP": 100000 }, 
      { "valueP": 150000 }, 
      { "valueP": 150000 }
    ];
    public _priceOptions = [
      { "valueP": null },  
      { "valueP": 500 }, 
      { "valueP": 1000 }, 
      { "valueP": 2000 }, 
      { "valueP": 3000 }, 
      { "valueP": 4000 }, 
      { "valueP": 5000 }, 
      { "valueP": 10000 }, 
      { "valueP": 20000 }, 
      { "valueP": 30000 }, 
      { "valueP": 40000 }, 
      { "valueP": 50000 }, 
      { "valueP": 60000 }, 
      { "valueP": 70000 },
      { "valueP": 80000 }, 
      { "valueP": 90000 }, 
      { "valueP": 100000 }, 
      { "valueP": 150000 }, 
      { "valueP": 150000 }
    ];
  
    constructor(
      // public appState: AppState,
      public router : Router,
      public fashionService : FashionService,
      public contentService : ContentService,
      public brandService : BrandService,
      public carouselService : CarouselService,
      private metaService: MetaService,
      public sanitizer: DomSanitizer
    ) {}
  
    public ngOnInit() {
      this.showAllProducts=false;
      this.priceMinFilter=500;
      this.priceMaxFilter=150000;
      this.isDiscounted=false;
      this.showFilteredData=false;
      window.scrollTo(0,0);
      console.log('hello `fashion` component');
      
      var urlBreakup = this.router.url.split("/");
      console.log(urlBreakup);
      
      this.pageType = urlBreakup[1]; 
      this.celebName=urlBreakup[2];
      
      if(this.celebName) {
        this.page = localStorage.getItem("page");
        this.industry=localStorage.getItem("Industry");
        this.celebrity = localStorage.getItem("celebrity");
        this.pagetype=localStorage.getItem('rootRoute');
        this.getDesignerBySlug(this.celebName);
        this.getProductsForDesigner(this.celebName);
      }
      if(!this.celebName){
        this.industry=localStorage.getItem("Industry");
        this.pagetype=localStorage.getItem('rootRoute');
        if(this.pageType =='celeb-store'){
          this.metaService.setTitle('Celebrity Clothes | Buy Indian Actress Actors Costumes Online');
          this.metaService.setTag('description','Buy Celebrity Dresses, Bollywood Sarees, Watches, Sunglasses, Jewellery, Bags, Shoes, Accessories, Actors Dresses, Actress Clothes, and much more only on Flikster Celebrity Store and Movie Store.');
          this.metaService.setTag('keywords','Actors Dresses For Sale, Actress Clothes, Bollywood Actors Dresses Online, Tollywood Actors Dresses Online, Actress Dresses Online, Actresses Dresses For Sale, Actors Clothes For Sale, Indian Celebrity Dresses Online, Celebrity Clothes Online, Actors Dresses Online');
          this.metaService.setTag('og:title','Buy Indian Celebrity Dresses Online | Actress Actors Costumes Online | Celebrity Clothes Online Shopping');
          this.metaService.setTag('og:description','Buy Celebrity Dresses, Bollywood Sarees, Watches, Sunglasses, Jewellery, Bags, Shoes, Accessories, Actors Dresses, Actress Clothes, and much more only on Flikster Celebrity Store and Movie Store.');
          this.metaService.setTag('og:keywords','Actors Dresses For Sale, Actress Clothes, Bollywood Actors Dresses Online, Tollywood Actors Dresses Online, Actress Dresses Online, Actresses Dresses For Sale, Actors Clothes For Sale, Indian Celebrity Dresses Online, Celebrity Clothes Online, Actors Dresses Online');
       
          // this.metaService.setTag('og:image',this.Auctionproduct.profilePic);
        }
        if(this.pageType =='movie-store'){
          this.metaService.setTitle('Buy Indian Celebrity Dresses at Flikster Movie Store');
          this.metaService.setTag('description','Bollywood Celebrity Dresses Collection, Tollywood Celebrity Dresses, Bollywood Replica Dresses, Bollywood Style Outfits, Telugu Movie Dresses, Actor Actress Clothes, Kollywood Movie Costumes, Tamil Film Dresses, Malayalam Movie Costumes, Kannada Movie Costumes, Latest Designer Dresses, Fancy Dresses, Hindi Movie clothes');
          this.metaService.setTag('keywords','Bollywood Movie Dresses, Tollywood Movie clothes, Kollywood Movie Costumes');
          this.metaService.setTag('og:title','Buy Indian Celebrity Dresses At Flikster Movie Store');
          this.metaService.setTag('og:description','Bollywood Celebrity Dresses Collection, Tollywood Celebrity Dresses, Bollywood Replica Dresses, Bollywood Style Outfits, Telugu Movie Dresses, Actor Actress Clothes, Kollywood Movie Costumes, Tamil Film Dresses, Malayalam Movie Costumes, Kannada Movie Costumes, Latest Designer Dresses, Fancy Dresses, Hindi Movie clothes');
          this.metaService.setTag('og:keywords','Bollywood Movie Dresses, Tollywood Movie clothes, Kollywood Movie Costumes');
       
          // this.metaService.setTag('og:image',this.Auctionproduct.profilePic);
        }
        if(this.pageType =='womenfashion'){
          this.metaService.setTitle('Buy Womens Fashion Clothing & Accessories Online at Low Prices In India');
          this.metaService.setTag('description','women dresses, sarees, ethnic wear, formal wear, western wear, nightwear, jewellery, Footwear, tops, jeans, lingerie, Ladies Apparels, party wear, casual wear, shoes, accessories,  tunics, Bags, Kurtis, Jackets, Skirts, trousers, sunglasses, dresses for girls, women clothing Online shopping sites india, womens apparels, fancy dresses and more only on flikster');
          this.metaService.setTag('keywords','Womens Desses, Online Shopping For Women, Dresses For Girls');
          this.metaService.setTag('og:title','Buy Womens Fashion Clothing And Accessories Online At Low Prices In India');
          this.metaService.setTag('og:description','women dresses, sarees, ethnic wear, formal wear, western wear, nightwear, jewellery, Footwear, tops, jeans, lingerie, Ladies Apparels, party wear, casual wear, shoes, accessories,  tunics, Bags, Kurtis, Jackets, Skirts, trousers, sunglasses, dresses for girls, women clothing Online shopping sites india, womens apparels, fancy dresses and more only on flikster');
          this.metaService.setTag('og:keywords','Womens Desses, Online Shopping For Women, Dresses For Girls');
       
          // this.metaService.setTag('og:image',this.Auctionproduct.profilePic);
        }
        if(this.pageType =='menfashion'){
          this.metaService.setTitle('Buy Mens Fashion Clothing & Accessories Online At Low Prices In India');
          this.metaService.setTag('description','casual shirts, Jeans, Tees, Pants, shoes, accessories, t shirts, slim-fit jeans, cotton shirts, striped tees, formal wear, western wear, ethnic wear, clothing, branded clothes, nightwear, Footwear, sunglasses, wallets, dresses for boys and more only on flikster, online shopping for men dresses, latest fashion for men');
          this.metaService.setTag('keywords','Men Dresses, Latest Fashion for men, Dresses for boys');
          this.metaService.setTag('og:title','Buy Mens Fashion Clothing And Accessories Online At Low Prices In India');
          this.metaService.setTag('og:description','casual shirts, Jeans, Tees, Pants, shoes, accessories, t shirts, slim-fit jeans, cotton shirts, striped tees, formal wear, western wear, ethnic wear, clothing, branded clothes, nightwear, Footwear, sunglasses, wallets, dresses for boys and more only on flikster, online shopping for men dresses, latest fashion for men');
          this.metaService.setTag('og:keywords','Men Dresses, Latest Fashion for men, Dresses for boys');
       
          // this.metaService.setTag('og:image',this.Auctionproduct.profilePic);
        }
        if(this.pageType =='designer-store'){
          this.metaService.setTitle('Buy Latest Designer Dresses for Women And Men');
          this.metaService.setTag('description','ethnic dresses, jewellery , sarees, salwar suits, lehenga choli, anarkali suits, Gowns, Designer Dresses Online, top designers in India, skirts, trousers, jackets, blouse, suit, kurti, Party Wear dresses, Latest Designer Brands, Dress Materials');
          this.metaService.setTag('keywords','Latest Designer Dresses, Designer Brands, top designers in India');
          this.metaService.setTag('og:title','Buy Latest Designer Dresses For Women & Men');
          this.metaService.setTag('og:description','ethnic dresses, jewellery , sarees, salwar suits, lehenga choli, anarkali suits, Gowns, Designer Dresses Online, top designers in India, skirts, trousers, jackets, blouse, suit, kurti, Party Wear dresses, Latest Designer Brands, Dress Materials');
          this.metaService.setTag('og:keywords','Latest Designer Dresses, Designer Brands, top designers in India');
       
          // this.metaService.setTag('og:image',this.Auctionproduct.profilePic);
        }
        if(this.pageType=='movie-store')
          this.getshopbyvideos('movie',this.industry);
         if(this.pageType=='celeb-store')
          this.getshopbyvideos('celeb',this.industry);
        //  if(this.pageType=='designer-store')
        //   this.getshopbyvideos('designer',this.industry);
       
        // if(this.pageType=='menfashion')
        //   this.getshopbyvideos('menfashion',this.industry);
        // if(this.pageType=='womenfashion')
        //   this.getshopbyvideos('womenfashion',this.industry);
      }

      if (urlBreakup[1]=="menfashion"){
        var fashionType = "men";
        this.getJustinProducts("menfashion");
        this.getClothing('menfashion');
        this.getBrands('menfashion');
        this.getFootware('menfashion');
        this.getAccessories('menfashion');
        this.getShirtsData('menfashion');
        // this.getDesignerWareProducts('menfashion');
      }
      if (urlBreakup[1]=="womenfashion"){
        var fashionType = "women";
        this.getJustinProducts("womenfashion");
        this.getClothing('womenfashion');
        this.getBrands('womenfashion');
        this.getFootware('womenfashion');
        this.getAccessories('womenfashion');
        this.getDesignerWareProducts('womenfashion');
      }
      if (urlBreakup[1]=="designer-store"){
        var fashionType = "designer";
      }
      if (urlBreakup[1]=="celeb-store"){
        var fashionType = "celebrity";
        this.getAuctions('celeb');
      }
      if (urlBreakup[1]=="movie-store"){
        var fashionType = "movie";
        this.getAuctions('movie');
        this.getSmallScreenProducts();
        this.getBigScreenProducts();
      }
      this.storeType = urlBreakup[1];

      this.getCategoryWidgets(this.storeType);
      this.getStealThatStyleForFashion(this.industry,fashionType);
      
      this.getShareYourStyle();
        // this.getTopContent("trailers");
        if(urlBreakup[1]=="movie-store" || urlBreakup[1]=="celeb-store")
         this.getBargain(this.pageType);

         if (this.pageType=='designer-store') {
           this.isDesigner = true;
           this.getAllDesigners();
           this.getDesignerItems('men');
           this.getDesignerItems('women');
         }
      localStorage.setItem("mainRoute","Fashion");
      localStorage.setItem("rootRoute",this.pageType);
      
      //see what i did there \m/
      this.rootRoute = localStorage.getItem("rootRoute");
      this.subRoute = localStorage.getItem("subRoute");
      
    }
    getJustinProducts(type){
      this.fashionService.getLatestProducts(type).subscribe(
        res => {
          if(res) {
            console.log(res);
            for(let i=0;i<res.hits.hits.length;i++){
              this.latestArrivedProducts.push(res.hits.hits[i]._source);
            }
            // this.getLatestProductsArray(this.latestArrivedProducts);
            this.getCarouselArray(this.latestArrivedProducts,4);
           
          }
        },
        error => {
          console.log("error getting content");
        }
      );

    }
    getSmallScreenProducts(){
      this.fashionService.getSmallScreenProds().subscribe(
        res => {
          if(res) {
            console.log(res);
            for(let i=0;i<res.hits.hits.length;i++){
              this.smallScreensProducts.push(res.hits.hits[i]._source);
            }
           
          }
        },
        error => {
          console.log("error getting content");
        }
      );

    }
    getBigScreenProducts(){
      this.bigScreensProducts=[];
      this.fashionService.getBigScreenProds(this.industry).subscribe(
        res => {
          if(res) {
            console.log(res);
            for(let i=0;i<res.hits.hits.length;i++){
              this.bigScreensProducts.push(res.hits.hits[i]._source);
            }
           
          }
        },
        error => {
          console.log("error getting content");
        }
      );

    }
    getAuctions(type){
      this.auctionData=[];
      this.fashionService.getAuctionData(type).subscribe(
        res => {
          if(res) {
            console.log(res);
            for(let i=0;i<res.hits.hits.length;i++){
              this.auctionData.push(res.hits.hits[i]._source);
            }
           
          }
          console.log(this.auctionData)
        },
        error => {
          console.log("error getting content");
        }
      );

    }
    setDisplayImage(item){
      if(item.imageGallery)
      this.displayImage=item.imageGallery[0];
      else
      this.displayImage=item.profilePic;

      return true;
    }
    getShirtsData(type){
      this.fashionService.getShirtsProducts(type).subscribe(
        res => {
          if(res) {
            console.log(res);
            for(let i=0;i<res.hits.hits.length;i++){
              this.latestShirtsProducts.push(res.hits.hits[i]._source);
            }
           
           
          }
        },
        error => {
          console.log("error getting content");
        }
      );

    }
    getDesignerWareProducts(type){
      this.fashionService.getDesignerProducts(type).subscribe(
        res => {
          if(res) {
            console.log(res);
            for(let i=0;i<res.hits.hits.length;i++){
              this.latestDesignerProducts.push(res.hits.hits[i]._source);
            }
            // this.getCarouselArray(this.latestArrivedProducts,4);
           
          }
        },
        error => {
          console.log("error getting content");
        }
      );

    }
    getAccessories(type){
      this.fashionService.getAccessories(type).subscribe(
        res => {
          if(res) {
            console.log(res);
            for(let i=0;i<res.hits.hits.length;i++){
              this.AccessoriesProducts.push(res.hits.hits[i]._source);
            }
            // this.getCarouselArray(this.latestArrivedProducts,4);
           
          }
        },
        error => {
          console.log("error getting content");
        }
      );

    }
    getFootware(type){
      this.fashionService.getFootwareProducts(type).subscribe(
        res => {
          if(res) {
            console.log(res);
            for(let i=0;i<res.hits.hits.length;i++){
              this.latestFootwareProducts.push(res.hits.hits[i]._source);
            }
            console.log(this.latestFootwareProducts);
            this.getCarouselFootwareArray(this.latestFootwareProducts,4);
           
          }
        },
        error => {
          console.log("error getting content");
        }
      );

    }
    getBrands(type){
      this.fashionService.getBrands(type).subscribe(
        res => {
          if(res) {
            console.log(res);
            for(let i=0;i<res.hits.hits.length;i++){
              if(this.brandArray.indexOf(res.hits.hits[i]._source.brand)==-1){
              this.brandArray.push(res.hits.hits[i]._source.brand);

            }
            }
            console.log(this.brandArray);
            for(let i=0;i<this.brandArray.length-1;i++){
             
              this.fashionService.getBrandDetails(this.brandArray[i]).subscribe(
                res => {
                  if(res) {
                    console.log(res);
                    this.brandDetailsArray.push(res.Items[0])
                  }
                },
                error => {
                  console.log("error getting content");
                }
              );

            }
            this.getBrandCarouselArray(this.brandDetailsArray);
           console.log(this.brandDetailsArray);
           
          }
        },
        error => {
          console.log("error getting content");
        }
      );

    }
    getClothing(type){
      this.fashionService.getClothingProducts(type).subscribe(
        res => {
          if(res) {
            console.log(res);
            for(let i=0;i<res.hits.hits.length;i++){
              this.latestClothingProducts.push(res.hits.hits[i]._source);
            }
            this.latestClothingProducts=this.latestClothingProducts.reverse();
            // this.getLatestProductsArray(this.latestArrivedProducts);
            this.getLatestProductsArray(this.latestClothingProducts);
           
          }
        },
        error => {
          console.log("error getting content");
        }
      );

    }
    getBrandCarouselArray(productArray){
      for(let i=0; i<2; i++) {
        if(productArray) {
          console.log(productArray);
          this.activeBrandDetailsArray.push(productArray[i]);
        }
      }
      if(productArray.length > 2) {
        var inactiveArrayCount = 0;
        for(let i=2; i<productArray.length; i++) {
          if(i%2 == 0) {
            this.inactiveBrandDetailsArrays.push(new Array());
            inactiveArrayCount++;
          }
          this.inactiveBrandDetailsArrays[Number(inactiveArrayCount-1)].push(productArray[i]);
        }
      }
      console.log(this.activeBrandDetailsArray);
      console.log(this.inactiveBrandDetailsArrays);
      this.inactiveBrandDetailsArrays = this.inactiveBrandDetailsArrays[0];

    }
    getLatestProductsArray(productArray){
      for(let i=0; i<4; i++) {
        if(productArray[i]) {
          console.log(productArray)
          console.log(productArray[i]);
          this.activeClothingArray.push(productArray[i]);
        }
      }
      if(productArray.length > 4) {
        var inactiveClothingArrayCount = 0;
        for(let i=4; i<productArray.length; i++) {
          if(i%4 == 0) {
            this.inactiveClothingArrays.push(new Array());
            inactiveClothingArrayCount++;
          }
          this.inactiveClothingArrays[Number(inactiveClothingArrayCount-1)].push(productArray[i]);
        }
      }
      console.log(this.activeArray);
      console.log(this.inactiveArrays);
      this.inactiveArray = this.inactiveArrays[0];

    }

    getCategoryWidgets(type) {
      this.fashionService.getCategoryWidgets(type).subscribe(
        res => {
          if(res) {
            console.log(res);
            this.widgets = res.hits.hits;
            // this.contentType = this.contents.contentType;
           // this.getRelatedFliks(this.contentType);
           console.log("***********widgets**********")
           console.log(this.widgets);
           for(let i=0;i<this.widgets.length;i++){
            if(this.widgets[i]._source.collectionType=='Icon Collection')
            this.shopByStar.push(this.widgets[i]._source);
          }
          console.log(this.shopByStar);
          }
        },
        error => {
          console.log("error getting content");
        }
      );
    }

  getProductsForDesigner(slug) {
    this.categoryArray=[];
      this.contentService.getProductsByTag(slug,'all').subscribe(
        res => {
          if(res) {
            console.log(res);
            this.designerProducts = res.hits.hits;
            // this.contentType = this.contents.contentType;
           // this.getRelatedFliks(this.contentType);
           console.log("***********designer products**********")
           console.log(this.designerProducts);
           for(let i=0;i<this.designerProducts.length;i++){
            if(this.designerProducts[i]._source.category[0]){
              if(this.designerProducts[i]._source.category[0][3]){
             if(this.categoryArray.indexOf(this.designerProducts[i]._source.category[0][3])==-1){
                console.log(this.designerProducts[i]._source.category[0][3]);
               this.categoryArray.push(this.designerProducts[i]._source.category[0][3]);
             }
              }
              if(!this.designerProducts[i]._source.category[0][3]){
               if(this.categoryArray.indexOf(this.designerProducts[i]._source.category[0][2])==-1){
                  this.categoryArray.push(this.designerProducts[i]._source.category[0][2]);
               }
                }
            }
          }
          console.log(this.categoryArray);
          }
        },
        error => {
          console.log("error getting content");
        }
      );
  }
  onPriceFromChange(index,type){
    
    console.log(index+type);
    if(type=='max'){
      var tempArray=this._priceOptions;
      this._minpriceOptions=[];
      for(let i=0;i<tempArray.length;i++){
        if(tempArray[i].valueP<index){
          this._minpriceOptions.push(tempArray[i]);
        }
      }
    }
    if(type=='min'){
      var tempArray=this._priceOptions;
      this._maxpriceOptions=[];
      for(let i=0;i<tempArray.length;i++){
        if(tempArray[i].valueP>index){
          this._maxpriceOptions.push(tempArray[i]);
        }
      }
    }
    console.log(this._maxpriceOptions);
    console.log(this.productsArray);
    var tempProductArray=this.productsArray;
    this.productsArray=[];
    for(let i=0;i<tempProductArray.length;i++){
      console.log(tempProductArray[i]);
      if(tempProductArray.discountObj)
      if(tempProductArray[i].isDiscount==true && this.priceMinFilter< +(tempProductArray.discountObj.discPrice)<this.priceMaxFilter)
{

this.productsArray.push(tempProductArray[i]);
}
if(tempProductArray[i].isDiscount==false && this.priceMinFilter< +(tempProductArray.price)<this.priceMaxFilter)
  {
  
  this.productsArray.push(tempProductArray[i]);
  }
    }

  }

  getDesignerItems(type) {
    this.designerMenProducts=[];
    this.designerWomenProducts=[];
      this.fashionService.getDesignerProductsByGender(type).subscribe(
        res => {
          if(res) {
            console.log(res);
            if(type == 'men')
              for(let i=0;i<res.hits.hits.length;i++){
            this.designerMenProducts.push(res.hits.hits[i]._source);
              }
            if(type == 'women')
              for(let i=0;i<res.hits.hits.length;i++){
            this.designerWomenProducts.push(res.hits.hits[i]._source);
              }
            // this.contentType = this.contents.contentType;
           // this.getRelatedFliks(this.contentType);
           console.log(this.designerWomenProducts);
           console.log(this.designerMenProducts);
          }
        },
        error => {
          console.log("error getting content");
        }
      );
  }
  showProductsOf(type){
    if(type !=this.fashionType)
    this.isDiscounted=false;
    this.fashionType=type;
    if(type=='men'){
     this.productsArray=this.designerMenProducts;
    }
    if(type=='women'){
      this.productsArray=this.designerWomenProducts;
    }

  }
  checkDiscounted(type){ 
    this.isDiscountedType=type;
    if(this.fashionType=='men'){
      this.productsArray=this.designerMenProducts;

    }
    if(this.fashionType=='women'){
      this.productsArray=this.designerWomenProducts;
    }
    if(type==false){
      this.tempArray=this.productsArray;
      this.productsArray=[];
      for(let i=0;i<this.tempArray.length;i++){
        if(this.tempArray[i].isDiscount==true){
          this.productsArray.push(this.tempArray[i])
        }
      }
    }
    else{
      this.productsArray=this.tempArray;
    }
    
    console.log(type);
  }

  getShareYourStyle() {
     this.fashionService.getContentForSYS().subscribe(
       res=> {
         if(res){
           this.shareYourStyleData =res.Items;
           console.log("************All SYS Content Data***************");
           console.log(this.shareYourStyleData);
          }
      },
      error => {
        console.log("error");
      }
    );
  }
  seemoreDesigners(){
    this.showType='Designers';
    this.showFilteredData=true;

  }
  backMenu(){
    this.showFilteredData=false;
  }
  filterDesignerData(item){
    this.showType=item;
    console.log(item);
    console.log('event emitted');
    this.showFilteredData=true;
    if(item=='Designers'){
      
    }
    if(item=='Boutiques'){
      this.getDesignersCollections();
    }
    if(item=='Categories' ||'Latest Arrivals'){
      // this.getCatagoryCollections();
      this.getDesignerProducts();
    }

  }
  filterIndustryData(item){
    console.log(item);
    var indus=localStorage.getItem('Industry');
    console.log(indus);
    if(item=='movie-store'){
      this.getBigScreenProducts();
      this.getshopbyvideos('movie',indus);
    this.getStealThatStyleForFashion(indus,'movie');
    }
    if(item=='celeb-store'){
      this.getshopbyvideos('celeb',indus);
    this.getStealThatStyleForFashion(indus,'celebrity');
    }
  }
  gotoMenCatagory(){
    this.showProductsOf('men');
    this.showFilteredData=true;
    // this.mencat=true;
    this.showType='Categories'

  }
  gotoWomenCatagory(){

    this.showProductsOf('women');
    this.showFilteredData=true;
    // this.womencat=true;
    this.showType='Categories'

  }
  viewAllProducts(products){
    this.allProducts=[];
    this.showAllProducts=true;
    for(let i=0;i<products.length;i++){
      this.allProducts.push(products[i]._source);
    }
    console.log(products);
  }
  backfashion(){
    this.showAllProducts=false;
  }

  getCarouselArray(data, num) {
    for(let i=0; i<num; i++) {
      if(data) {
        console.log(data);
        this.activeArray.push(data[i]);
      }
    }
    if(data.length > num) {
      var inactiveArrayCount = 0;
      for(let i=num; i<data.length; i++) {
        if(i%num == 0) {
          this.inactiveArrays.push(new Array());
          inactiveArrayCount++;
        }
        this.inactiveArrays[Number(inactiveArrayCount-1)].push(data[i]);
      }
    }
    console.log(this.activeArray);
    console.log(this.inactiveArrays);
    this.inactiveArray = this.inactiveArrays[0];
  }
  getCarouselFootwareArray(data, num) {
    for(let i=0; i<num; i++) {
      if(data) {
        console.log(data);
        this.activeFootwareArray.push(data[i]);
      }
    }
    if(data.length > num) {
      var inactiveArrayCount = 0;
      for(let i=num; i<data.length; i++) {
        if(i%num == 0) {
          this.inactiveFootwareArrays.push(new Array());
          inactiveArrayCount++;
        }
        this.inactiveFootwareArrays[Number(inactiveArrayCount-1)].push(data[i]);
      }
    }
    console.log(this.activeFootwareArray);
    console.log(this.inactiveFootwareArrays);
    this.inactiveFootwareArrays = this.inactiveFootwareArrays[0];
  }
  
    getStealThatStyleForFashion(industry,type){ 
      this.stealThatStyleData=null; 
      this.fashionService.stealThatStyleFashion(industry,type).subscribe(
        res => {
          console.log(res);
          if(res){
            this.stealThatStyleData = res.hits.hits;
          }
        },
        error => {
          console.log("Error in stealThatStyle");
        }

      );
    }
    getCatagoryCollections(){
      this.carouselService.getAllCatagoryCollections().subscribe(
        res => {
          console.log(res);
          if(res){
            this.widgets = res.hits.hits;
            console.log(this.widgets);
            
            // this.carouselData = res.Items;  
            // console.log("***********Designere****************");
            // console.log(this.carouselData);
          }
        },
        error => {
          console.log("Error in carouselDesiner");
        }
  
      );

    }
    getDesignerProducts(){
      this.productsArray=[];
      this.carouselService.getAllDesignerProducts().subscribe(
        res => {
          console.log(res);
          if(res){
            for(let i=0;i<res.hits.hits.length;i++){
              this.productsArray.push(res.hits.hits[i]._source);
            }
           console.log(this.productsArray);
          }
        },
        error => {
          console.log("Error in carouselDesiner");
        }
  
      );

    }
    goToSeeDesigner(page,celebrity) {
      localStorage.setItem("page",page);
      localStorage.setItem("celebrity",celebrity);
      this.router.navigate([page,celebrity]);  
     
       
    }
    getDesignersCollections(){
      this.carouselService.getAllDesignerCollections().subscribe(
        res => {
          console.log(res);
          if(res){
            this.widgets = res.hits.hits;
            // this.carouselData = res.Items;  
            // console.log("***********Designere****************");
            // console.log(this.carouselData);
          }
        },
        error => {
          console.log("Error in carouselDesiner");
        }
  
      );

    }
    getAllDesigners(){  
      this.carouselService.getCarouselBySlug().subscribe(
        res => {
          console.log(res);
          if(res){
            this.carouselData = res.Items;  
            console.log("***********Designere****************");
            console.log(this.carouselData);
          }
        },
        error => {
          console.log("Error in carouselDesiner");
        }
  
      );
    }
    getshopbyvideos(page,industry){
      this.shopbyVideosData=[];
      this.fashionService.getshopbyvideos(page,industry).subscribe(
        res => {
          console.log("THIS IS shopbyvideos DATA");
          console.log(res);
          for(let i=0;i<res.hits.hits.length;i++){
          this.shopbyVideosData.push(res.hits.hits[i]._source);
        }
          console.log(this.shopbyVideosData);

          // if(res){
            
          //     this.pagecontent = res;
          // }
        },
        error => {
          console.log("Error in getting top content");
        }
      );


    }
    setUrl(url){
      console.log(url);
      if(url.split('/')[2] == "www.youtube.com") {
        if(url.split('/')[3] == "embed") {
          this.videoUrl = url;
        } else {
          this.videoUrl = "https://www.youtube.com/embed/" + url.split('/')[3].split('=')[1];
        }
      } else if(url.split('/')[2] == "www.dailymotion.com") {
          
      } else {
        this.videoUrl = "https://www.youtube.com/embed/" + url.split('/')[3];
      }
      console.log(this.videoUrl);
      return true;

    }
    getDesignerBySlug(id){
      this.fashionService.getDesignerBySlug(id).subscribe(
        res => {
          console.log("THIS IS DESIGNER DATA");
          console.log(res);

          if(res){
            
              this.pagecontent = res.Items[0];
          }
        },
        error => {
          console.log("Error in getting top content");
        }
      );
    }

    getBargain(pageType){  
      this.fashionService.bargain(pageType).subscribe(
        res => {
          console.log(res);
          if(res){
            for(let i=0;i<res.hits.hits.length;i++){
            this.bargainData.push(res.hits.hits[i]._source);
          }
            this.getCarouselArray(this.bargainData,4);
          }
        },
        error => {
          console.log("Error in bargain");
        }

      );
    }

    // getTopContent(type) {
    //   this.contentService.getTopContent(type).subscribe(
    //     res => {
    //       console.log(res);
    //       if(res){
    //           this.topTrailers = res;
    //       }
    //     },
    //     error => {
    //       console.log("Error in getting top content");
    //     }
    //   );
    // }
    goToSeeAll(first,second,third) {
      localStorage.setItem("first",first);
      localStorage.setItem("second",second);
      localStorage.setItem("third",third);
      this.router.navigate([first,second,third]);  
    }
    togglefunction(){
    this.see=!this.see;

    }
    goToClothing() {
      localStorage.removeItem('category');
      localStorage.setItem("subPage","Clothing");
      if (this.rootRoute=="womenfashion"){
        localStorage.setItem("items","15");
      }
      if (this.rootRoute=="menfashion"){
        localStorage.setItem("items","7");
      }
      this.router.navigate([this.rootRoute,this.subRoute]);  
    }

    // getDesignerById(){
    //   this.fashionService.designer().subscribe(
    //     res => {
    //       console.log("THIS IS DESIGNER DATA");
    //       console.log(res);
    //       if(res){
    //           this.brand = res;
    //       }
    //     },
    //     error => {
    //       console.log("Error in getting top content");
    //     }
    //   );
    // }

    public submitState(value: string) {
      console.log('submitState', value);
      // this.appState.set('value', value);
      this.localState.value = '';
    }

    ngOnDestroy() {
      console.log("cwdcniwoivnovinvwicnowc");
    }
  }
  