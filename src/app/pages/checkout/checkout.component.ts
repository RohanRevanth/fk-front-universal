 
  import { 
    Component, Input,OnInit,
    trigger, state, animate, transition, style 
   } from '@angular/core';
  import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
  //import { AppState } from '../../app.service';
  import { ActivatedRoute, Router } from "@angular/router";
  import { ContentService } from '../../models/content/content.service';
  import { ProductService } from "../../models/product/product.service";
  import { TransactionService } from "../../models/transaction/transaction.service";
import { validateConfig } from '@angular/router/src/config';
import {Location} from '@angular/common';
declare var $:any;

  @Component({
    
    selector: 'checkout', 
    providers: [ ],
    styleUrls: [ './checkout.component.css' ],
    templateUrl: './checkout.component.html',
    animations: [
      trigger('visibility', [
          state('shown', style({
              opacity: 1
          })),
          state('hidden', style({  display: 'none',
              opacity: 0
          })),
          transition('* => *', animate('.5s'))
      ],
      
  ),
        trigger('Debit', [
        state('shown', style({
            opacity: 1
        })),
        state('hidden', style({  display: 'none',
            opacity: 0
        })),
        transition('* => *', animate('.5s'))
    ],

),
        trigger('Credit', [
          state('shown', style({
              opacity: 1
          })),
          state('hidden', style({  display: 'none',
              opacity: 0
          })),
          transition('* => *', animate('.5s'))
        ],

),
          trigger('Net', [
            state('shown', style({
                opacity: 1
            })),
            state('hidden', style({  display: 'none',
                opacity: 0
            })),
            transition('* => *', animate('.5s'))
          ],

          ),
              ],
    
  })
  export class CheckoutComponent implements OnInit {
    proceedPayment: boolean;
    displayResultedText: string;
    couponCode: any;
    public shippingCost :number=0;
    bilpinInvailid: boolean;
    bilmobInvailid: boolean =false;
    shippinInvalid: boolean =false;
    shipmobInvailid: boolean =false;
    userObject: string;
    editingProduct: boolean =false;
    bilstate: boolean = false;
    bilpin: boolean =false;
    bilmob: boolean =false;
    bilcity: boolean =false;
    biladd: boolean =false ;
    bilname: boolean =false;
    shipstate: boolean = false;
    shippin: boolean =false;
    shipmob: boolean =false;
    shipcity: boolean =false;
    shipname: boolean =false;
    shipadd: boolean =false;
    isAuctionProduct: boolean = false;
    auctionSlug: string;
    displayAddressRequireds: boolean;
    gef: string;
    paymentType: any;
    displayAddressRequired: boolean=false;
    username: string;
    productOrder: any;
    orderId: any;
    item: any;
    public displayLoginPopup : boolean = false
    public def:any;
    public message :any;
    public sendObject: any;
    public totalCost: number;
    public cartItems: any;
    public today: number = Date.now();
    public deliveryDate:any = Date.now();
    public orderproduct: any;
    public slug: string;
    public localState = { value: '' };
    public statusCode:any;
    public get :boolean = false;
    
    visibility = 'hidden';
    Debit= 'hidden';
    Credit= 'hidden';
    Net= 'hidden';
    order : boolean = false;
    shippingAddressOrder = {};
    shippingAddress = {
      name : "",
      address : "",
      city : "",
      state : "",
      pincode: null,
      mobile: null,
      email: null,
      type:"",
    };
    public billingAddress ={
      name : "",
      address : "",
      city : "",
      state : "",
      pincode: null,
      mobile: null,
      email:null,
      type:"",
    };
    shippingAddressSet : boolean = false;
    showBillingAddress :boolean =false;
    billingAddressSet :boolean =true;
    payment_id;
    payment_request_id;
    isPaymentSucess : boolean;
    
    toggleVisibility() {
      this.hideAll();
      this.visibility = 'shown';
    }
    toggleDebit() {
      this.hideAll();
      this.Debit =  'shown';
    }
    toggleCredit(){
      this.hideAll();
      this.Credit =  'shown';
    }
    toggleNet(){
      this.hideAll();
      this.Net =  'shown';
    }
    
    constructor(
      private _location: Location,
     // public appState: AppState,
      public router: Router,
      private activatedRoute: ActivatedRoute,
      private contentService: ContentService,
      private productService : ProductService,
      private transactionService : TransactionService,
    ) {}
  
    public ngOnInit() {
$(document).ready(function() {
  $('.use-billing-adrs').click(function(){
  $(this).toggleClass('check');
  });
});

     var userId = localStorage.getItem('username');
    //  if(!userId){
    //    console.log('login');
    //    localStorage.setItem("displayLoginInIndustryPage","true");
       
    // }
      window.scrollTo(0,0);
      //this.billingAddress= this.getEmptyDiscObj();
        // this.billingAddress=[
        // {name : "",
        // address : "",
        // city : "",
        // state : "",
        // pincode: "",
        // mobile: "",
        // }];
      this.deliveryDate = new Date();
      
      this.deliveryDate.setDate(this.deliveryDate.getDate()+15);
      // console.log('hello `contentpage` component');
      var urlBreakup = this.router.url.split("/");
      console.log(urlBreakup);
      if(urlBreakup[3]){
        var loggedin=localStorage.getItem('isLoggedIn');
        if(loggedin=='false'){
          // localStorage.setItem("preLogin","auction/checkout/"+urlBreakup[3]);
          this.displayLoginPopup=true;
        }
        else{
        this.slug=urlBreakup[3];
        this.isAuctionProduct=true;
      }
      }
      else{
      this.slug = urlBreakup[2];
      console.log(this.slug);
      }
      //console.log(this.orderId);
    this.username=localStorage.getItem('name');
      this.activatedRoute.queryParams.subscribe(params => {
        this.payment_id = params['payment_id'];
        this.payment_request_id = params['payment_request_id'];
        //console.log(this.orderproduct.id);
        console.log(this.orderId);
        if(this.payment_id && this.payment_request_id) {
          this.transactionService.verifyPayment(this.payment_id,this.payment_request_id)
          .subscribe(
            response =>{
              this.order =true;
              if(!response){
                this.isPaymentSucess = false; 
              }else{
                if(response.result.payment_request.payment.status == "Credit"){
                  this.isPaymentSucess = true;
                  this.paymentType = response.result.payment_request.payment.instrument_type;
                }else{
                  this.isPaymentSucess = false;
                }
              }
          },
          error => {
            this.order =true;
            this.isPaymentSucess = false;
            console.log("error getting payment");
          });
        }
    });

      this.getOrder(this.slug);
      
    }

    getOrder(slug) {
      this.productService.getOrderById(slug).subscribe(
       res => {
         console.log("Order service");
         console.log(res);
         var identity=localStorage.getItem('username');
          if(res.userId==identity){
            this.orderproduct = res;
            this.cartItems = res.product;
            this.shippingAddressOrder=res.shippingAddress;
            this.totalCost = 0;
            if(this.cartItems)
            for(let i=0; i<this.cartItems.length; i++) {
              this.totalCost = this.totalCost + Number(this.cartItems[i].price*this.cartItems[i].quantity);
            }}
            else
              {
                this.router.navigate(['home']);
              }
            // this.totalCost = this.totalCost+100  
       },
       error => {
         console.log("error getting product");
       }
     );
  
    //  this.topMovies = this.topMoviesMock;
    //this.product = this.productMock;
   }
  //  getEmptyDiscObj(){
  //   return {
  //     name : "",
  //     address : "",
  //     city : "",
  //     state : "",
  //     pincode: "",
  //     mobile: "",

  //   };
  //  }
  closeLoginPopup () {
    console.log("event emmitted");
    this.displayLoginPopup = false; 
  }
   inputAddress() {
    console.log(this.shippingAddress.mobile);
     
    // console.log(this.shippingAddress);
    if(this.shippingAddress.name=='')
      this.shipname=true;
    else
      this.shipname=false;
    if(this.shippingAddress.address=='')
      this.shipadd=true;
    else
      this.shipadd=false;
    if(this.shippingAddress.city=='')
      this.shipcity=true;
    else
      this.shipcity=false;
    if(this.shippingAddress.mobile==null)
      this.shipmob=true;
    else
      this.shipmob=false;
      if(this.shippingAddress.mobile!=null)
      if(this.shippingAddress.mobile<999999999){
      this.shipmobInvailid=true;
      console.log(this.shipmobInvailid);
   }
    else
      this.shipmobInvailid=false;
    if(this.shippingAddress.pincode==null)
      this.shippin=true;
    else
      this.shippin=false;
      if(this.shippingAddress.pincode!=null){
      if(this.shippingAddress.pincode<99999){
      this.shippinInvalid=true;
      }
    }
    else
      this.shippinInvalid=false;
    if(this.shippingAddress.state=='')
      this.shipstate=true;
    else
      this.shipstate=false;
     if(this.shippingAddress.name=='' || this.shippingAddress.address=="" ||
    this.shippingAddress.city=="" || this.shippingAddress.mobile==null
    || this.shippingAddress.pincode==null || this.shippingAddress.state=="" || this.shipmobInvailid==true || this.shippinInvalid==true){
     this.shippingAddressSet = false;
     var abc = 'Please enter mandatory fields ';
     this.def = abc;

    }
     else
     this.shippingAddressSet = true;
     console.log(this.shippingAddress);
     console.log (this.shippingAddress.name);
   }
   inputbillingAddress(){
    if(this.billingAddress.name=='')
      this.bilname=true;
    else
      this.bilname=false;
    if(this.billingAddress.address=='')
      this.biladd=true;
    else
      this.biladd=false;
    if(this.billingAddress.city=='')
      this.bilcity=true;
    else
      this.bilcity=false;
    if(this.billingAddress.mobile==null)
      this.bilmob=true;
    else
      this.bilmob=false;
      if(this.billingAddress.mobile!=null)
      if(this.billingAddress.mobile<999999999){
        this.bilmobInvailid=true;
        console.log(this.shipmobInvailid);
     }
      else
        this.bilmobInvailid=false;
    if(this.billingAddress.pincode==null)
      this.bilpin=true;
    else
      this.bilpin=false;
      if(this.billingAddress.pincode!=null)
      if(this.billingAddress.pincode<99999){
        this.bilpinInvailid=true;
        console.log(this.shipmobInvailid);
     }
      else
        this.bilpinInvailid=false;
    if(this.billingAddress.state=='')
      this.bilstate=true;
    else
      this.bilstate=false;
    if(this.billingAddress.name=='' || this.billingAddress.address=="" ||
    this.billingAddress.city=="" || this.billingAddress.mobile==null
    || this.billingAddress.pincode==null || this.billingAddress.state=="" || this.bilmobInvailid==true || this.bilpinInvailid==true){
     this.billingAddressSet = false;
     var message = 'Please enter mandatory fields ';
     this.gef = message;

    }
     else
     this.billingAddressSet=true;
     console.log(this.billingAddressSet);

   }

   editAddress() {
     this.shippingAddressSet = false;
     console.log(this.shippingAddress);
   }
   editbillingAddress(){
    this.billingAddressSet = false;
    console.log(this.billingAddress);

   }
   changeAddress(){
    this.shippingAddress={name : "",
    address : "",
    city : "",
    state : "",
    pincode: null,
    mobile: null,
    email:null,
    type:"",
  }
    this.shippingAddressSet = false;

   }
   conformAddress(){
    
     this.proceedPayment=true;
     
   }
   changebillingAddress(){
    this.billingAddress={ name : "",
    address : "",
    city : "",
    state : "",
    pincode: null,
    mobile: null,
    email:null,
    type:"",
}
    this.billingAddressSet = false;
     
   }

    hideAll() {
      this.visibility = 'hidden';
      this.Debit =  'hidden';
      this.Credit = 'hidden';
      this.Net = 'hidden';
    }
    private changingOrder(changedOrder)
    {
      if(changedOrder){
        console.log(changedOrder);
        this.getOrder(this.slug);
        this.editingProduct=false;
        this.get = false; 
       
      }
       else
       this.editingProduct=false
       this.get = false; 
      }

    orderConformed(type){
      if(type == "COD")
        this.orderproduct.status = "PLACED";
      else
        this.orderproduct.status = "pending";
        console.log(this.shippingAddress);
      if(this.billingAddressSet && this.shippingAddressSet)
      {
      this.orderproduct.billingAddress = this.billingAddress;
      this.orderproduct.shippingAddress = this.shippingAddress;
      console.log(this.orderproduct.shippingAddress);
      this.orderproduct.paymentMode = type;
      this.updateOrder(this.orderproduct,this.slug);
      this.displayAddressRequireds=true;
     }
     else {
       console.log(this.billingAddressSet);
       console.log(this.shippingAddressSet);

       this.displayAddressRequired=true;
       
     }
    }

    updateOrder(order,id) {
      
      console.log(order);
      
      this.productService.updateOrders(order,id).subscribe(
       res => {
         console.log("updateOrder service");
         console.log(res);
          if(res) {
            console.log(res);
           if(this.orderproduct.paymentMode == "ONLINE")
             this.instamojoPay();
          }
       },
       error => {
         console.log("error updateOrder product");
       }
     );
   }

    orderClose(){
      this.order =false;
       this.router.navigate(['home']); 
    }

    paymentDataObj() {
      var userObject=JSON.parse(localStorage.getItem("userObj"));
      return {
          "orderId": this.slug,
          "phone": this.shippingAddress.mobile,
          "email":  userObject.email,
          "buyer": localStorage.getItem("username"),
          "buyer_name": localStorage.getItem("name"),
          "amount": this.totalCost + this.shippingCost,
          "purpose": "FLIKSTER-PRODUCT PURCHASE",
          "status": "",
          "send_sms": false,
          "send_email": false,
          "sms_status": "Pending",
          "email_status": "Pending",
          "shorturl": null,
          "redirect_url": "http://flikster.com/checkout/"+this.slug,
          "webhook": "http://www.example.com/webhook/",
          "allow_repeated_payments": false,
      };
    }
    
    getapp(item){
      this.editingProduct=true;
      this.item=item;
      console.log(this.orderproduct);
      console.log(this.orderproduct.product[item]);
      this.sendObject=this.orderproduct.product[item];
      this.productOrder=this.orderproduct;
      this.orderId=this.orderproduct.id;
      this.get = true; 
    }

    useBillingAddress(){
      console.log('llllll');
      this.showBillingAddress =!this.showBillingAddress;
      if(this.showBillingAddress==false){
        this.billingAddress=this.shippingAddress;
        this.billingAddressSet=true;
      }
      else
      this.billingAddressSet=false;
      // if(this.showBillingAddress=true){
      //   this.billingAddressSet=false;
      // }
      
    }
    removeItem(index){
      this.orderproduct.product.splice(index, 1);
      this.removeProduct(this.orderproduct,this.slug);
      if(this.orderproduct.product.length==0){
        this._location.back();
      }
    }
    goBack(){
      this._location.back();
    }
    tryAgain(){
      this.router.navigateByUrl('/dummy', {skipLocationChange: true}).then(()=>
      this.router.navigate([this.router.url]));
    }
    backToHome(){
      this.router.navigateByUrl('/dummy', {skipLocationChange: true}).then(()=>
      this.router.navigate(['home']));
    }
    applyCoupon(){
      if(this.couponCode){
        this.displayResultedText='Invalid Coupon Code'
        this.couponCode=null;
      }
    }
    removeProduct(order,id){
      this.productService.updateOrders(order, id).subscribe(
        res => {
          console.log("updateOrder service");
          console.log(res);
          if (res) {
            console.log(res);
            if (res.statusCode == 200) {
              this.getOrder(this.slug);
            }
  
  
          }
  
        },
        error => {
          console.log("error updateOrder product");
        }
      );

    }

    public submitState(value: string) {
      console.log('submitState', value);
     // this.appState.set('value', value);
      this.localState.value = '';
    }

    instamojoPay() {
    //   this.transactionService.getTokenForPayment().subscribe(
    //       res => {
    //         console.log(res);
    //       },
    //       error => {
    //         console.log("Error in posting transaction");
    //       });


      var body = this.paymentDataObj();
      this.transactionService.createTransaction(body).subscribe(
          res => {
            console.log(res);
            if(res.success) {
              if(res.payment_request.longurl) {
                let paymentUrl = res.payment_request.longurl;
                console.log(paymentUrl);
                window.open(paymentUrl,"_self");
                // window.location.href = paymentUrl;
              }
            }
          },
          error => {
            console.log("Error in posting transaction");
          }
    );
  }

  requestInvoice(){
    let isRequestInvoice = {
      orderId : this.orderproduct.id,
      name : localStorage.getItem("name"),

    };
console.log(isRequestInvoice);
    this.transactionService.requestInvoice(isRequestInvoice).subscribe(
        res => {
            if(res) {
              
              if(res.statusCode == 200) {
                this.message="Request has been sent Successfully"
               
              } else {
                console.log(res.message);
              }
            }
          },
          error => {
            console.log("Error");
          }
    );
  
}

  }
  